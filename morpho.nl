<?xml version="1.0" encoding="UTF-8"?>
<description lang="nl">
  <!-- INVARIABLE -->
  <table name="inv" rads="..*" fast="-">
    <form suffix="" tag=""/>
  </table>
  <table name="pref" rads="..*" fast="-">
    <form suffix="_" tag=""/>
  </table>
  <table name="a1" rads=".*">
    <!-- 363 members -->
    <form  suffix="" tag="dverb"/>
  </table>
  <table name="a2" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="" tag="dv_tag"/>
  </table>
  <table name="a3" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="pp_noun"/>
  </table>
  <table name="a4" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="dv_tag"/>
    <form  suffix="" tag="dverb"/>
  </table>
  <table name="adjective1" rads=".*">
    <!-- 2548 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="e" tag="adjectivege_e"/>
  </table>
  <table name="adjective2" rads=".*">
    <!-- 1169 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="e" tag="adjectivee"/>
  </table>
  <table name="adjective3" rads=".*">
    <!-- 1032 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective4" rads=".*">
    <!-- 839 members -->
    <form  suffix="" tag="adjectivege_both"/>
  </table>
  <table name="adjective5" rads=".*">
    <!-- 198 members -->
    <form  suffix="" tag="adjectiveboth"/>
  </table>
  <table name="adjective6" rads=".*">
    <!-- 173 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="t" tag="adjectivest"/>
    <form  suffix="te" tag="adjectiveste"/>
  </table>
  <table name="adjective7" rads=".*">
    <!-- 96 members -->
    <form  suffix="" tag="adjectivepred"/>
  </table>
  <table name="adjective8" rads=".*">
    <!-- 87 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective9" rads=".*">
    <!-- 77 members -->
    <form  suffix="" tag="adjectivestof"/>
  </table>
  <table name="adjective10" rads=".*">
    <!-- 74 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="" tag="adjectiveno_e,subject_vp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,subject_sbar"/>
    <form  suffix="e" tag="adjectivee,subject_vp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,subject_sbar"/>
    <form  suffix="er" tag="adjectiveer,subject_vp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,subject_sbar"/>
    <form  suffix="ere" tag="adjectiveere,subject_vp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,subject_sbar"/>
    <form  suffix="st" tag="adjectivest,subject_vp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,subject_sbar"/>
    <form  suffix="ste" tag="adjectiveste,subject_vp"/>
  </table>
  <table name="adjective11" rads=".*">
    <!-- 71 members -->
    <form  suffix="ar" tag="adjectiveno_e"/>
    <form  suffix="arder" tag="adjectiveer"/>
    <form  suffix="ardere" tag="adjectiveere"/>
    <form  suffix="arst" tag="adjectivest"/>
    <form  suffix="arste" tag="adjectiveste"/>
    <form  suffix="re" tag="adjectivee"/>
  </table>
  <table name="adjective12" rads=".*">
    <!-- 67 members -->
    <form  suffix="al" tag="adjectiveno_e"/>
    <form  suffix="le" tag="adjectivee"/>
  </table>
  <table name="adjective13" rads=".*">
    <!-- 63 members -->
    <form  suffix="an" tag="adjectivege_no_e"/>
    <form  suffix="ne" tag="adjectivege_e"/>
  </table>
  <table name="adjective14" rads=".*">
    <!-- 61 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="te" tag="adjectivege_e"/>
  </table>
  <table name="adjective15" rads=".*">
    <!-- 58 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,pp"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,pp"/>
  </table>
  <table name="adjective16" rads=".*">
    <!-- 58 members -->
    <form  suffix="ar" tag="adjectiveno_e"/>
    <form  suffix="re" tag="adjectivee"/>
  </table>
  <table name="adjective17" rads=".*">
    <!-- 56 members -->
    <form  suffix="os" tag="adjectiveno_e"/>
    <form  suffix="ost" tag="adjectivest"/>
    <form  suffix="oste" tag="adjectiveste"/>
    <form  suffix="ze" tag="adjectivee"/>
    <form  suffix="zer" tag="adjectiveer"/>
    <form  suffix="zere" tag="adjectiveere"/>
  </table>
  <table name="adjective18" rads=".*">
    <!-- 52 members -->
    <form  suffix="" tag="adjectiveend"/>
    <form  suffix="e" tag="adjectiveende"/>
  </table>
  <table name="adjective19" rads=".*">
    <!-- 47 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="der" tag="adjectiveer"/>
    <form  suffix="dere" tag="adjectiveere"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective20" rads=".*">
    <!-- 46 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
  </table>
  <table name="adjective21" rads=".*">
    <!-- 46 members -->
    <form  suffix="seerd" tag="adjectivege_no_e"/>
    <form  suffix="seerde" tag="adjectivege_e"/>
    <form  suffix="zeerd" tag="adjectivege_no_e"/>
    <form  suffix="zeerde" tag="adjectivege_e"/>
  </table>
  <table name="adjective22" rads=".*">
    <!-- 40 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,subject_sbar"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,subject_sbar"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,subject_sbar"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,subject_sbar"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,subject_sbar"/>
  </table>
  <table name="adjective23" rads=".*">
    <!-- 40 members -->
    <form  suffix="el" tag="adjectiveno_e"/>
    <form  suffix="le" tag="adjectivee"/>
  </table>
  <table name="adjective24" rads=".*">
    <!-- 39 members -->
    <form  suffix="os" tag="adjectiveno_e"/>
    <form  suffix="ze" tag="adjectivee"/>
  </table>
  <table name="adjective25" rads=".*">
    <!-- 36 members -->
    <form  suffix="" tag="adjectiveprefix"/>
  </table>
  <table name="adjective26" rads=".*">
    <!-- 36 members -->
    <form  suffix="f" tag="adjectiveno_e"/>
    <form  suffix="fst" tag="adjectivest"/>
    <form  suffix="fste" tag="adjectiveste"/>
    <form  suffix="ve" tag="adjectivee"/>
    <form  suffix="ver" tag="adjectiveer"/>
    <form  suffix="vere" tag="adjectiveere"/>
  </table>
  <table name="adjective27" rads=".*">
    <!-- 33 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,pp"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
  </table>
  <table name="adjective28" rads=".*">
    <!-- 32 members -->
    <form  suffix="al" tag="adjectiveno_e"/>
    <form  suffix="alst" tag="adjectivest"/>
    <form  suffix="alste" tag="adjectiveste"/>
    <form  suffix="le" tag="adjectivee"/>
    <form  suffix="ler" tag="adjectiveer"/>
    <form  suffix="lere" tag="adjectiveere"/>
  </table>
  <table name="adjective29" rads=".*">
    <!-- 31 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="ere" tag="adjectiveere"/>
  </table>
  <table name="adjective30" rads=".*">
    <!-- 31 members -->
    <form  suffix="es" tag="adjectiveno_e"/>
    <form  suffix="se" tag="adjectivee"/>
  </table>
  <table name="adjective31" rads=".*" fast="-">
    <!-- 29 members -->
    <form  suffix="f" tag="adjectiveno_e"/>
    <form  suffix="ve" tag="adjectivee"/>
  </table>
  <table name="adjective32" rads=".*" fast="-">
    <!-- 28 members -->
    <form  suffix="" tag="adjectivege_both"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective33" rads=".*" fast="-">
    <!-- 28 members -->
    <form  suffix="s" tag="adjectiveno_e"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ze" tag="adjectivee"/>
    <form  suffix="zer" tag="adjectiveer"/>
    <form  suffix="zere" tag="adjectiveere"/>
  </table>
  <table name="adjective34" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="" tag="adjectivege_both"/>
    <form  suffix="" tag="adjectivege_both,pp"/>
  </table>
  <table name="adjective35" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="" tag="adjectiveboth"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective36" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="" tag="adjectiveno_e,subject_vp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
    <form  suffix="e" tag="adjectivee,subject_sbar"/>
    <form  suffix="e" tag="adjectivee,subject_vp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="er" tag="adjectiveer,subject_sbar"/>
    <form  suffix="er" tag="adjectiveer,subject_vp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="ere" tag="adjectiveere,subject_sbar"/>
    <form  suffix="ere" tag="adjectiveere,subject_vp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="st" tag="adjectivest,subject_sbar"/>
    <form  suffix="st" tag="adjectivest,subject_vp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
    <form  suffix="ste" tag="adjectiveste,subject_sbar"/>
    <form  suffix="ste" tag="adjectiveste,subject_vp"/>
  </table>
  <table name="adjective37" rads=".*" fast="-">
    <!-- 26 members -->
    <form  suffix="am" tag="adjectiveno_e"/>
    <form  suffix="amst" tag="adjectivest"/>
    <form  suffix="amste" tag="adjectiveste"/>
    <form  suffix="me" tag="adjectivee"/>
    <form  suffix="mer" tag="adjectiveer"/>
    <form  suffix="mere" tag="adjectiveere"/>
  </table>
  <table name="adjective38" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="" tag="adjectiveend"/>
    <form  suffix="e" tag="adjectiveende"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective39" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
  </table>
  <table name="adjective40" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="le" tag="adjectivee"/>
    <form  suffix="ler" tag="adjectiveer"/>
    <form  suffix="lere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective41" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="te" tag="adjectivee"/>
    <form  suffix="ter" tag="adjectiveer"/>
    <form  suffix="tere" tag="adjectiveere"/>
  </table>
  <table name="adjective42" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,so_np"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,so_np"/>
  </table>
  <table name="adjective43" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="" tag="adjectivepostn_no_e"/>
    <form  suffix="e" tag="adjectivee"/>
  </table>
  <table name="adjective44" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="el" tag="adjectiveno_e"/>
    <form  suffix="elst" tag="adjectivest"/>
    <form  suffix="elste" tag="adjectiveste"/>
    <form  suffix="le" tag="adjectivee"/>
    <form  suffix="ler" tag="adjectiveer"/>
    <form  suffix="lere" tag="adjectiveere"/>
  </table>
  <table name="adjective45" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,subject_vp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,subject_vp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,subject_vp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,subject_vp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,subject_vp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,subject_vp"/>
  </table>
  <table name="adjective46" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="" tag="adjectiveno_e,subject_vp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,subject_sbar"/>
    <form  suffix="e" tag="adjectivee,subject_vp"/>
  </table>
  <table name="adjective47" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,subject_sbar"/>
  </table>
  <table name="adjective48" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="ed" tag="adjectivege_no_e"/>
    <form  suffix="de" tag="adjectivege_e"/>
  </table>
  <table name="adjective49" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="" tag="adjectivege_both"/>
    <form  suffix="" tag="adjectivege_both,so_np"/>
  </table>
  <table name="adjective50" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="ar" tag="adjectiveno_e"/>
    <form  suffix="ar" tag="adjectiveno_e,pp"/>
    <form  suffix="arder" tag="adjectiveer"/>
    <form  suffix="arder" tag="adjectiveer,pp"/>
    <form  suffix="ardere" tag="adjectiveere"/>
    <form  suffix="ardere" tag="adjectiveere,pp"/>
    <form  suffix="arst" tag="adjectivest"/>
    <form  suffix="arst" tag="adjectivest,pp"/>
    <form  suffix="arste" tag="adjectiveste"/>
    <form  suffix="arste" tag="adjectiveste,pp"/>
    <form  suffix="re" tag="adjectivee"/>
    <form  suffix="re" tag="adjectivee,pp"/>
  </table>
  <table name="adjective51" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="eel" tag="adjectiveno_e"/>
    <form  suffix="ële" tag="adjectivee"/>
  </table>
  <table name="adjective52" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="s" tag="adjectiveno_e"/>
    <form  suffix="ze" tag="adjectivee"/>
  </table>
  <table name="adjective53" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="adjectivege_both"/>
    <form  suffix="" tag="adjectivege_both,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
  </table>
  <table name="adjective54" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="fe" tag="adjectivee"/>
    <form  suffix="fer" tag="adjectiveer"/>
    <form  suffix="fere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective55" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="te" tag="adjectivee"/>
  </table>
  <table name="adjective56" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="le" tag="adjectivee"/>
  </table>
  <table name="adjective57" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="ar" tag="adjectiveno_e"/>
    <form  suffix="ar" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="arder" tag="adjectiveer"/>
    <form  suffix="arder" tag="adjectiveer,subject_sbar"/>
    <form  suffix="ardere" tag="adjectiveere"/>
    <form  suffix="ardere" tag="adjectiveere,subject_sbar"/>
    <form  suffix="arst" tag="adjectivest"/>
    <form  suffix="arst" tag="adjectivest,subject_sbar"/>
    <form  suffix="arste" tag="adjectiveste"/>
    <form  suffix="arste" tag="adjectiveste,subject_sbar"/>
    <form  suffix="re" tag="adjectivee"/>
    <form  suffix="re" tag="adjectivee,subject_sbar"/>
  </table>
  <table name="adjective58" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="ctief" tag="adjectiveno_e"/>
    <form  suffix="ctiefst" tag="adjectivest"/>
    <form  suffix="ctiefste" tag="adjectiveste"/>
    <form  suffix="ctieve" tag="adjectivee"/>
    <form  suffix="ctiever" tag="adjectiveer"/>
    <form  suffix="ctievere" tag="adjectiveere"/>
    <form  suffix="ktief" tag="adjectiveno_e"/>
    <form  suffix="ktiefst" tag="adjectivest"/>
    <form  suffix="ktiefste" tag="adjectiveste"/>
    <form  suffix="ktieve" tag="adjectivee"/>
    <form  suffix="ktiever" tag="adjectiveer"/>
    <form  suffix="ktievere" tag="adjectiveere"/>
  </table>
  <table name="adjective59" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="adjectivee"/>
    <form  suffix="" tag="adjectiveno_e"/>
  </table>
  <table name="adjective60" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,er_pp_sbar"/>
    <form  suffix="" tag="adjectivege_no_e,er_pp_vp"/>
    <form  suffix="" tag="adjectivege_no_e,pp"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,pp"/>
  </table>
  <table name="adjective61" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,er_pp_sbar"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,er_pp_sbar"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,er_pp_sbar"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
  </table>
  <table name="adjective62" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="" tag="adjectiveno_e,subject_vp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
    <form  suffix="e" tag="adjectivee,subject_vp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="er" tag="adjectiveer,subject_vp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="ere" tag="adjectiveere,subject_vp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="st" tag="adjectivest,subject_vp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
    <form  suffix="ste" tag="adjectiveste,subject_vp"/>
  </table>
  <table name="adjective63" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="adjectivepred"/>
    <form  suffix="" tag="adjectivepred,pp"/>
  </table>
  <table name="adjective64" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="an" tag="adjectiveno_e"/>
    <form  suffix="ne" tag="adjectivee"/>
  </table>
  <table name="adjective65" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,transitive"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,transitive"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,transitive"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,transitive"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,transitive"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,transitive"/>
  </table>
  <table name="adjective66" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="ke" tag="adjectivee"/>
    <form  suffix="ker" tag="adjectiveer"/>
    <form  suffix="kere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective67" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="se" tag="adjectivee"/>
    <form  suffix="ser" tag="adjectiveer"/>
    <form  suffix="sere" tag="adjectiveere"/>
    <form  suffix="t" tag="adjectivest"/>
    <form  suffix="te" tag="adjectiveste"/>
  </table>
  <table name="adjective68" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ar" tag="adjectiveno_e"/>
    <form  suffix="ar" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="ar" tag="adjectiveno_e,subject_vp"/>
    <form  suffix="arder" tag="adjectiveer"/>
    <form  suffix="arder" tag="adjectiveer,subject_sbar"/>
    <form  suffix="arder" tag="adjectiveer,subject_vp"/>
    <form  suffix="ardere" tag="adjectiveere"/>
    <form  suffix="ardere" tag="adjectiveere,subject_sbar"/>
    <form  suffix="ardere" tag="adjectiveere,subject_vp"/>
    <form  suffix="arst" tag="adjectivest"/>
    <form  suffix="arst" tag="adjectivest,subject_sbar"/>
    <form  suffix="arst" tag="adjectivest,subject_vp"/>
    <form  suffix="arste" tag="adjectiveste"/>
    <form  suffix="arste" tag="adjectiveste,subject_sbar"/>
    <form  suffix="arste" tag="adjectiveste,subject_vp"/>
    <form  suffix="re" tag="adjectivee"/>
    <form  suffix="re" tag="adjectivee,subject_sbar"/>
    <form  suffix="re" tag="adjectivee,subject_vp"/>
  </table>
  <table name="adjective69" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="on" tag="adjectiveno_e"/>
    <form  suffix="ne" tag="adjectivee"/>
    <form  suffix="ner" tag="adjectiveer"/>
    <form  suffix="nere" tag="adjectiveere"/>
    <form  suffix="onst" tag="adjectivest"/>
    <form  suffix="onste" tag="adjectiveste"/>
  </table>
  <table name="adjective70" rads=".*(gesproken|gedragen|gekomen|geroepen|gegrepen|aangelopen)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="adjectivege_both"/>
    <form  suffix="" tag="adjectivege_both,fixed"/>
  </table>
  <table name="adjective71" rads=".*(getroost|gekwetst|beheerst|aangepast|toegespitst|verontrust)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,pp"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
  </table>
  <table name="adjective72" rads=".*(alert|doodziek|afhankelijk|aansprakelijk|vermaard|zenuwachtig)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,er_pp_sbar"/>
    <form  suffix="" tag="adjectiveno_e,er_pp_vp"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,er_pp_sbar"/>
    <form  suffix="er" tag="adjectiveer,er_pp_vp"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,er_pp_sbar"/>
    <form  suffix="st" tag="adjectivest,er_pp_vp"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
  </table>
  <table name="adjective73" rads=".*(tragisch|problematisch|realistisch|ironisch|fantastisch|dramatisch)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="" tag="adjectiveno_e,subject_vp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,subject_sbar"/>
    <form  suffix="e" tag="adjectivee,subject_vp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,subject_sbar"/>
    <form  suffix="er" tag="adjectiveer,subject_vp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,subject_sbar"/>
    <form  suffix="ere" tag="adjectiveere,subject_vp"/>
    <form  suffix="t" tag="adjectivest"/>
    <form  suffix="t" tag="adjectivest,subject_sbar"/>
    <form  suffix="t" tag="adjectivest,subject_vp"/>
    <form  suffix="te" tag="adjectiveste"/>
    <form  suffix="te" tag="adjectiveste,subject_sbar"/>
    <form  suffix="te" tag="adjectiveste,subject_vp"/>
  </table>
  <table name="adjective74" rads=".*(sponta|urba|inhuma|profa|huma|zelfvolda)" fast="-">
    <!-- 6 members -->
    <form  suffix="an" tag="adjectiveno_e"/>
    <form  suffix="anst" tag="adjectivest"/>
    <form  suffix="anste" tag="adjectiveste"/>
    <form  suffix="ne" tag="adjectivee"/>
    <form  suffix="ner" tag="adjectiveer"/>
    <form  suffix="nere" tag="adjectiveere"/>
  </table>
  <table name="adjective75" rads=".*(rabia|proba|separa|obliga|priva|despera)" fast="-">
    <!-- 6 members -->
    <form  suffix="at" tag="adjectiveno_e"/>
    <form  suffix="te" tag="adjectivee"/>
  </table>
  <table name="adjective76" rads=".*(commerci|materi|immateri|artifici|parti|offici)" fast="-">
    <!-- 6 members -->
    <form  suffix="eel" tag="adjectiveno_e"/>
    <form  suffix="eelst" tag="adjectivest"/>
    <form  suffix="eelste" tag="adjectiveste"/>
    <form  suffix="elere" tag="adjectiveere"/>
    <form  suffix="ële" tag="adjectivee"/>
    <form  suffix="ëler" tag="adjectiveer"/>
  </table>
  <table name="adjective77" rads=".*(ongeme|sere|obsce|algeme|heteroge|homoge)" fast="-">
    <!-- 6 members -->
    <form  suffix="en" tag="adjectiveno_e"/>
    <form  suffix="enst" tag="adjectivest"/>
    <form  suffix="enste" tag="adjectiveste"/>
    <form  suffix="ne" tag="adjectivee"/>
    <form  suffix="ner" tag="adjectiveer"/>
    <form  suffix="nere" tag="adjectiveere"/>
  </table>
  <table name="adjective78" rads=".*(oestroge|endoge|allerge|schizofre|transge|hallucinoge)" fast="-">
    <!-- 6 members -->
    <form  suffix="en" tag="adjectiveno_e"/>
    <form  suffix="ne" tag="adjectivee"/>
  </table>
  <table name="adjective79" rads=".*(knalro|bloedro|donkerro|vuurro|wijnro|hoogro)" fast="-">
    <!-- 6 members -->
    <form  suffix="od" tag="adjectiveno_e"/>
    <form  suffix="de" tag="adjectivee"/>
    <form  suffix="der" tag="adjectiveer"/>
    <form  suffix="dere" tag="adjectiveere"/>
    <form  suffix="odst" tag="adjectivest"/>
    <form  suffix="odste" tag="adjectiveste"/>
  </table>
  <table name="adjective80" rads=".*(polyfo|asynchro|wonderscho|allochto|francofo|synchro)" fast="-">
    <!-- 6 members -->
    <form  suffix="on" tag="adjectiveno_e"/>
    <form  suffix="ne" tag="adjectivee"/>
  </table>
  <table name="adjective81" rads=".*(gewijd|toegewijd|uitgebracht|toegevoegd|geleverd)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,so_np"/>
    <form  suffix="" tag="adjectivege_no_e,so_pp"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,so_np"/>
    <form  suffix="e" tag="adjectivege_e,so_pp"/>
  </table>
  <table name="adjective82" rads=".*(geschud|afgeschud|gewed|ingebed|gered)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="de" tag="adjectivege_e"/>
  </table>
  <table name="adjective83" rads=".*(onwelgevallig|toekomend|onwelkom|onwelgezind|welgezind)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,so_np"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,so_np"/>
  </table>
  <table name="adjective84" rads=".*(krom|tam|lam|stram|klam)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="me" tag="adjectivee"/>
    <form  suffix="mer" tag="adjectiveer"/>
    <form  suffix="mere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective85" rads=".*(slap|hip|krap|rap|sip)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="pe" tag="adjectivee"/>
    <form  suffix="per" tag="adjectiveer"/>
    <form  suffix="pere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective86" rads=".*(bereikba|verenigba|onvindba|converteerba|leverba)" fast="-">
    <!-- 5 members -->
    <form  suffix="ar" tag="adjectiveno_e"/>
    <form  suffix="ar" tag="adjectiveno_e,pp"/>
    <form  suffix="re" tag="adjectivee"/>
    <form  suffix="re" tag="adjectivee,pp"/>
  </table>
  <table name="adjective87" rads=".*(aantoonba|onvoorspelba|onbespreekba|betwistba|bespreekba)" fast="-">
    <!-- 5 members -->
    <form  suffix="ar" tag="adjectiveno_e"/>
    <form  suffix="ar" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="re" tag="adjectivee"/>
    <form  suffix="re" tag="adjectivee,subject_sbar"/>
  </table>
  <table name="adjective88" rads=".*(verla|bepra|aangepra|doorgepra|nagepra)" fast="-">
    <!-- 5 members -->
    <form  suffix="at" tag="adjectivege_no_e"/>
    <form  suffix="te" tag="adjectivege_e"/>
  </table>
  <table name="adjective89" rads=".*(perfe|indire|abstra|defe|inta)" fast="-">
    <!-- 5 members -->
    <form  suffix="ct" tag="adjectiveno_e"/>
    <form  suffix="cte" tag="adjectivee"/>
    <form  suffix="cter" tag="adjectiveer"/>
    <form  suffix="ctere" tag="adjectiveere"/>
    <form  suffix="ctst" tag="adjectivest"/>
    <form  suffix="ctste" tag="adjectiveste"/>
    <form  suffix="kt" tag="adjectiveno_e"/>
    <form  suffix="kte" tag="adjectivee"/>
    <form  suffix="kter" tag="adjectiveer"/>
    <form  suffix="ktere" tag="adjectiveere"/>
    <form  suffix="ktst" tag="adjectivest"/>
    <form  suffix="ktste" tag="adjectiveste"/>
  </table>
  <table name="adjective90" rads=".*(dedu|intersubje|rea|intera|respe)" fast="-">
    <!-- 5 members -->
    <form  suffix="ctief" tag="adjectiveno_e"/>
    <form  suffix="ctieve" tag="adjectivee"/>
    <form  suffix="ktief" tag="adjectiveno_e"/>
    <form  suffix="ktieve" tag="adjectivee"/>
  </table>
  <table name="adjective91" rads=".*(reë|irreë|irratione|sensatione|ratione)" fast="-">
    <!-- 5 members -->
    <form  suffix="el" tag="adjectiveno_e"/>
    <form  suffix="el" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="el" tag="adjectiveno_e,subject_vp"/>
    <form  suffix="elst" tag="adjectivest"/>
    <form  suffix="elst" tag="adjectivest,subject_sbar"/>
    <form  suffix="elst" tag="adjectivest,subject_vp"/>
    <form  suffix="elste" tag="adjectiveste"/>
    <form  suffix="elste" tag="adjectiveste,subject_sbar"/>
    <form  suffix="elste" tag="adjectiveste,subject_vp"/>
    <form  suffix="le" tag="adjectivee"/>
    <form  suffix="le" tag="adjectivee,subject_sbar"/>
    <form  suffix="le" tag="adjectivee,subject_vp"/>
    <form  suffix="ler" tag="adjectiveer"/>
    <form  suffix="ler" tag="adjectiveer,subject_sbar"/>
    <form  suffix="ler" tag="adjectiveer,subject_vp"/>
    <form  suffix="lere" tag="adjectiveere"/>
    <form  suffix="lere" tag="adjectiveere,subject_sbar"/>
    <form  suffix="lere" tag="adjectiveere,subject_vp"/>
  </table>
  <table name="adjective92" rads=".*(halfdo|ongelo|ongeno|sno|infraro)" fast="-">
    <!-- 5 members -->
    <form  suffix="od" tag="adjectiveno_e"/>
    <form  suffix="de" tag="adjectivee"/>
  </table>
  <table name="adjective93" rads=".*(begro|vergro|uitvergro|ontblo|uitgelo)" fast="-">
    <!-- 5 members -->
    <form  suffix="ot" tag="adjectivege_no_e"/>
    <form  suffix="te" tag="adjectivege_e"/>
  </table>
  <table name="adjective94" rads=".*(zu|gu|du|pu|ongu)" fast="-">
    <!-- 5 members -->
    <form  suffix="ur" tag="adjectiveno_e"/>
    <form  suffix="re" tag="adjectivee"/>
    <form  suffix="urder" tag="adjectiveer"/>
    <form  suffix="urdere" tag="adjectiveere"/>
    <form  suffix="urst" tag="adjectivest"/>
    <form  suffix="urste" tag="adjectiveste"/>
  </table>
  <table name="adjective95" rads=".*(onweersproken|onomstreden|omstreden|privé)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectiveboth"/>
    <form  suffix="" tag="adjectiveboth,subject_sbar"/>
  </table>
  <table name="adjective96" rads=".*(voormalig|deeltijds|forensisch|voltijds)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectiveboth"/>
    <form  suffix="e" tag="adjectivee"/>
  </table>
  <table name="adjective97" rads=".*(geïnteresseerd|gericht|beroemd|bedroefd)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,er_pp_sbar"/>
    <form  suffix="" tag="adjectivege_no_e,er_pp_vp"/>
    <form  suffix="" tag="adjectivege_no_e,pp"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,er_pp_sbar"/>
    <form  suffix="er" tag="adjectiveer,er_pp_vp"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,er_pp_sbar"/>
    <form  suffix="st" tag="adjectivest,er_pp_vp"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
  </table>
  <table name="adjective98" rads=".*(ontgoocheld|verbaasd|ontstemd|verbijsterd)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,er_pp_sbar"/>
    <form  suffix="" tag="adjectivege_no_e,object_sbar"/>
    <form  suffix="" tag="adjectivege_no_e,pp"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,er_pp_sbar"/>
    <form  suffix="er" tag="adjectiveer,object_sbar"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,er_pp_sbar"/>
    <form  suffix="st" tag="adjectivest,object_sbar"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
  </table>
  <table name="adjective99" rads=".*(gesneld|gebracht|veroordeeld|gebeurd)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,fixed"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,fixed"/>
  </table>
  <table name="adjective100" rads=".*(verontwaardigd|bezorgd|benieuwd|verwonderd)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,object_sbar"/>
    <form  suffix="" tag="adjectivege_no_e,pp"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,object_sbar"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,object_sbar"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
  </table>
  <table name="adjective101" rads=".*(geacht|gebeukt|verklaard|verondersteld)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,pred"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,pred"/>
  </table>
  <table name="adjective102" rads=".*(onthutst|gehaast|gekuist|beslist)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="ere" tag="adjectiveere"/>
  </table>
  <table name="adjective103" rads=".*(verrot|uitgeput|ontzet|verhit)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="te" tag="adjectivege_e"/>
    <form  suffix="ter" tag="adjectiveer"/>
    <form  suffix="tere" tag="adjectiveere"/>
  </table>
  <table name="adjective104" rads=".*(funest|ongerust|veelvuldig|rechtlijnig)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
  </table>
  <table name="adjective105" rads=".*(onjuist|ongepast|juist|triest)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="" tag="adjectiveno_e,subject_vp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,subject_sbar"/>
    <form  suffix="e" tag="adjectivee,subject_vp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,subject_sbar"/>
    <form  suffix="er" tag="adjectiveer,subject_vp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,subject_sbar"/>
    <form  suffix="ere" tag="adjectiveere,subject_vp"/>
  </table>
  <table name="adjective106" rads=".*(bar|star|schor|dor)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="der" tag="adjectiveer"/>
    <form  suffix="dere" tag="adjectiveere"/>
    <form  suffix="re" tag="adjectivee"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective107" rads=".*(gescha|versma|geba|gewa)" fast="-">
    <!-- 4 members -->
    <form  suffix="ad" tag="adjectivege_no_e"/>
    <form  suffix="de" tag="adjectivege_e"/>
  </table>
  <table name="adjective108" rads=".*(Zuid|Oost|West|Noord)" fast="-">
    <!-- 4 members -->
    <form  suffix="afrikaans" tag="adjectiveno_e"/>
    <form  suffix="-Afrikaans" tag="adjectiveno_e"/>
    <form  suffix="-Afrikaanse" tag="adjectivee"/>
    <form  suffix="afrikaanse" tag="adjectivee"/>
  </table>
  <table name="adjective109" rads=".*(gesta|grijpgra|schietgra|praatgra)" fast="-">
    <!-- 4 members -->
    <form  suffix="ag" tag="adjectiveno_e"/>
    <form  suffix="ge" tag="adjectivee"/>
  </table>
  <table name="adjective110" rads=".*(polyga|eerza|monoga|handza)" fast="-">
    <!-- 4 members -->
    <form  suffix="am" tag="adjectiveno_e"/>
    <form  suffix="me" tag="adjectivee"/>
  </table>
  <table name="adjective111" rads=".*(desola|la|korda|para)" fast="-">
    <!-- 4 members -->
    <form  suffix="at" tag="adjectiveno_e"/>
    <form  suffix="atst" tag="adjectivest"/>
    <form  suffix="atste" tag="adjectiveste"/>
    <form  suffix="te" tag="adjectivee"/>
    <form  suffix="ter" tag="adjectiveer"/>
    <form  suffix="tere" tag="adjectiveere"/>
  </table>
  <table name="adjective112" rads=".*(auto|bureau|sociaal-demo|aristo)" fast="-">
    <!-- 4 members -->
    <form  suffix="cratisch" tag="adjectiveno_e"/>
    <form  suffix="cratische" tag="adjectivee"/>
    <form  suffix="cratischer" tag="adjectiveer"/>
    <form  suffix="cratischere" tag="adjectiveere"/>
    <form  suffix="cratischt" tag="adjectivest"/>
    <form  suffix="cratischte" tag="adjectiveste"/>
    <form  suffix="kratisch" tag="adjectiveno_e"/>
    <form  suffix="kratische" tag="adjectivee"/>
    <form  suffix="kratischer" tag="adjectiveer"/>
    <form  suffix="kratischere" tag="adjectiveere"/>
    <form  suffix="kratischt" tag="adjectivest"/>
    <form  suffix="kratischte" tag="adjectiveste"/>
  </table>
  <table name="adjective113" rads=".*(West|Noord|Zuid|Oost)" fast="-">
    <!-- 4 members -->
    <form  suffix="europees" tag="adjectiveno_e"/>
    <form  suffix="-Europees" tag="adjectiveno_e"/>
    <form  suffix="-Europese" tag="adjectivee"/>
    <form  suffix="europese" tag="adjectivee"/>
  </table>
  <table name="adjective114" rads=".*(xenofo|anaëro|homofo|aëro)" fast="-">
    <!-- 4 members -->
    <form  suffix="ob" tag="adjectiveno_e"/>
    <form  suffix="be" tag="adjectivee"/>
  </table>
  <table name="adjective115" rads=".*(ho|kurkdro|dro|torenho)" fast="-">
    <!-- 4 members -->
    <form  suffix="og" tag="adjectiveno_e"/>
    <form  suffix="ge" tag="adjectivee"/>
    <form  suffix="ger" tag="adjectiveer"/>
    <form  suffix="gere" tag="adjectiveere"/>
    <form  suffix="ogst" tag="adjectivest"/>
    <form  suffix="ogste" tag="adjectiveste"/>
  </table>
  <table name="adjective116" rads=".*(genoeg|voldoende|onvoldoende)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectiveboth"/>
    <form  suffix="" tag="adjectiveboth,object_sbar"/>
    <form  suffix="" tag="adjectiveboth,object_vp"/>
    <form  suffix="" tag="adjectiveboth,subject_sbar"/>
    <form  suffix="" tag="adjectiveboth,subject_vp"/>
  </table>
  <table name="adjective117" rads=".*(bezeten|uitgekeken|doordrongen)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivege_both"/>
    <form  suffix="" tag="adjectivege_both,er_pp_sbar"/>
    <form  suffix="" tag="adjectivege_both,er_pp_vp"/>
    <form  suffix="" tag="adjectivege_both,pp"/>
  </table>
  <table name="adjective118" rads=".*(geworden|gebakken|geheten)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivege_both"/>
    <form  suffix="" tag="adjectivege_both,pred"/>
  </table>
  <table name="adjective119" rads=".*(gewaand|getint|ingekleurd)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,ap_pred"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,ap_pred"/>
  </table>
  <table name="adjective120" rads=".*(voorbereid|verguld|verzekerd)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,er_pp_sbar"/>
    <form  suffix="" tag="adjectivege_no_e,pp"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,pp"/>
  </table>
  <table name="adjective121" rads=".*(bevoorrecht|geschokt|vereerd)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,object_sbar"/>
    <form  suffix="" tag="adjectivege_no_e,object_vp"/>
    <form  suffix="e" tag="adjectivege_e"/>
  </table>
  <table name="adjective122" rads=".*(bezet|opgezet|ingezet)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,pp"/>
    <form  suffix="te" tag="adjectivege_e"/>
    <form  suffix="te" tag="adjectivege_e,pp"/>
  </table>
  <table name="adjective123" rads=".*(bedankt|geheid|aanvaard)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,subject_sbar"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,subject_sbar"/>
  </table>
  <table name="adjective124" rads=".*(meegerekend|gesigneerd|gestift)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivege_no_e"/>
    <form  suffix="" tag="adjectivege_no_e,transitive"/>
    <form  suffix="e" tag="adjectivege_e"/>
    <form  suffix="e" tag="adjectivege_e,transitive"/>
  </table>
  <table name="adjective125" rads=".*(gepikeerd|pissig|stomverbaasd)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,er_pp_sbar"/>
    <form  suffix="" tag="adjectiveno_e,object_sbar"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
  </table>
  <table name="adjective126" rads=".*(ongeschikt|bezig|rijp)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,er_pp_vp"/>
    <form  suffix="" tag="adjectiveno_e,object_vp"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,er_pp_vp"/>
    <form  suffix="er" tag="adjectiveer,object_vp"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,er_pp_vp"/>
    <form  suffix="st" tag="adjectivest,object_vp"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
  </table>
  <table name="adjective127" rads=".*(onmachtig|giftig|onbevoegd)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,object_vp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,object_vp"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,object_vp"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective128" rads=".*(ernstig|gunstig|ongunstig)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
    <form  suffix="e" tag="adjectivee,subject_sbar"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="er" tag="adjectiveer,subject_sbar"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="ere" tag="adjectiveere,subject_sbar"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="st" tag="adjectivest,subject_sbar"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
    <form  suffix="ste" tag="adjectiveste,subject_sbar"/>
  </table>
  <table name="adjective129" rads=".*(belangrijk|mogelijk|onduidelijk)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,pp"/>
    <form  suffix="" tag="adjectiveno_e,subject_sbar_no_het"/>
    <form  suffix="" tag="adjectiveno_e,subject_vp_no_het"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,pp"/>
    <form  suffix="e" tag="adjectivee,subject_sbar_no_het"/>
    <form  suffix="e" tag="adjectivee,subject_vp_no_het"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,pp"/>
    <form  suffix="er" tag="adjectiveer,subject_sbar_no_het"/>
    <form  suffix="er" tag="adjectiveer,subject_vp_no_het"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,pp"/>
    <form  suffix="ere" tag="adjectiveere,subject_sbar_no_het"/>
    <form  suffix="ere" tag="adjectiveere,subject_vp_no_het"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,pp"/>
    <form  suffix="st" tag="adjectivest,subject_sbar_no_het"/>
    <form  suffix="st" tag="adjectivest,subject_vp_no_het"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,pp"/>
    <form  suffix="ste" tag="adjectiveste,subject_sbar_no_het"/>
    <form  suffix="ste" tag="adjectiveste,subject_vp_no_het"/>
  </table>
  <table name="adjective130" rads=".*(ontwijkend|welgevallig|onwaardig)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,so_np"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,so_np"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="er" tag="adjectiveer,so_np"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="ere" tag="adjectiveere,so_np"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,so_np"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,so_np"/>
  </table>
  <table name="adjective131" rads=".*(fair|stoer|revolutionair)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="" tag="adjectiveno_e,subject_sbar"/>
    <form  suffix="" tag="adjectiveno_e,subject_vp"/>
    <form  suffix="der" tag="adjectiveer"/>
    <form  suffix="der" tag="adjectiveer,subject_sbar"/>
    <form  suffix="der" tag="adjectiveer,subject_vp"/>
    <form  suffix="dere" tag="adjectiveere"/>
    <form  suffix="dere" tag="adjectiveere,subject_sbar"/>
    <form  suffix="dere" tag="adjectiveere,subject_vp"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="e" tag="adjectivee,subject_sbar"/>
    <form  suffix="e" tag="adjectivee,subject_vp"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="st" tag="adjectivest,subject_sbar"/>
    <form  suffix="st" tag="adjectivest,subject_vp"/>
    <form  suffix="ste" tag="adjectiveste"/>
    <form  suffix="ste" tag="adjectiveste,subject_sbar"/>
    <form  suffix="ste" tag="adjectiveste,subject_vp"/>
  </table>
  <table name="adjective132" rads=".*(stug|vlug|log)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectiveno_e"/>
    <form  suffix="ge" tag="adjectivee"/>
    <form  suffix="ger" tag="adjectiveer"/>
    <form  suffix="gere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective133" rads=".*(loodrecht|onvergelijkelijk|afzonderlijk)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivepostn_no_e"/>
    <form  suffix="e" tag="adjectivee"/>
    <form  suffix="er" tag="adjectiveer"/>
    <form  suffix="ere" tag="adjectiveere"/>
    <form  suffix="st" tag="adjectivest"/>
    <form  suffix="ste" tag="adjectiveste"/>
  </table>
  <table name="adjective134" rads=".*(tegen|gebaat|oneens)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivepred"/>
    <form  suffix="" tag="adjectivepred,er_pp_sbar"/>
    <form  suffix="" tag="adjectivepred,er_pp_vp"/>
    <form  suffix="" tag="adjectivepred,pp"/>
  </table>
  <table name="adjective135" rads=".*(netjes|zonde|doodzonde)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="adjectivepred"/>
    <form  suffix="" tag="adjectivepred,subject_sbar"/>
    <form  suffix="" tag="adjectivepred,subject_vp"/>
  </table>
  <table name="adjective136" rads=".*(gesma|ontza|retrogra)" fast="-">
    <!-- 3 members -->
    <form  suffix="ad" tag="adjectiveno_e"/>
    <form  suffix="de" tag="adjectivee"/>
  </table>
  <table name="adjective137" rads=".*(la|va|tra)" fast="-">
    <!-- 3 members -->
    <form  suffix="ag" tag="adjectiveno_e"/>
    <form  suffix="agst" tag="adjectivest"/>
    <form  suffix="agste" tag="adjectiveste"/>
    <form  suffix="ge" tag="adjectivee"/>
    <form  suffix="ger" tag="adjectiveer"/>
    <form  suffix="gere" tag="adjectiveere"/>
  </table>
  <table name="adjective138" rads=".*(va|lukra|ra)" fast="-">
    <!-- 3 members -->
    <form  suffix="ak" tag="adjectiveno_e"/>
    <form  suffix="akst" tag="adjectivest"/>
    <form  suffix="akste" tag="adjectiveste"/>
    <form  suffix="ke" tag="adjectivee"/>
    <form  suffix="ker" tag="adjectiveer"/>
    <form  suffix="kere" tag="adjectiveere"/>
  </table>
  <table name="adjective139" rads=".*(zorgza|bekwa|spaarza)" fast="-">
    <!-- 3 members -->
    <form  suffix="am" tag="adjectiveno_e"/>
    <form  suffix="am" tag="adjectiveno_e,pp"/>
    <form  suffix="amst" tag="adjectivest"/>
    <form  suffix="amst" tag="adjectivest,pp"/>
    <form  suffix="amste" tag="adjectiveste"/>
    <form  suffix="amste" tag="adjectiveste,pp"/>
    <form  suffix="me" tag="adjectivee"/>
    <form  suffix="me" tag="adjectivee,pp"/>
    <form  suffix="mer" tag="adjectiveer"/>
    <form  suffix="mer" tag="adjectiveer,pp"/>
    <form  suffix="mere" tag="adjectiveere"/>
    <form  suffix="mere" tag="adjectiveere,pp"/>
  </table>
  <table name="adjective140" rads=".*(onpra|dida|ta)" fast="-">
    <!-- 3 members -->
    <form  suffix="ctisch" tag="adjectiveno_e"/>
    <form  suffix="ctische" tag="adjectivee"/>
    <form  suffix="ctischer" tag="adjectiveer"/>
    <form  suffix="ctischere" tag="adjectiveere"/>
    <form  suffix="ctischt" tag="adjectivest"/>
    <form  suffix="ctischte" tag="adjectiveste"/>
    <form  suffix="ktisch" tag="adjectiveno_e"/>
    <form  suffix="ktische" tag="adjectivee"/>
    <form  suffix="ktischer" tag="adjectiveer"/>
    <form  suffix="ktischere" tag="adjectiveere"/>
    <form  suffix="ktischt" tag="adjectivest"/>
    <form  suffix="ktischte" tag="adjectiveste"/>
  </table>
  <table name="adjective141" rads=".*(bre|hemelsbre|le)" fast="-">
    <!-- 3 members -->
    <form  suffix="ed" tag="adjectiveno_e"/>
    <form  suffix="de" tag="adjectivee"/>
    <form  suffix="der" tag="adjectiveer"/>
    <form  suffix="dere" tag="adjectiveere"/>
    <form  suffix="edst" tag="adjectivest"/>
    <form  suffix="edste" tag="adjectiveste"/>
  </table>
  <table name="adjective142" rads=".*(vieren|zevenen|vijfen)" fast="-">
    <!-- 3 members -->
    <form  suffix="eenhalf" tag="adjectiveno_e"/>
    <form  suffix="eenhalve" tag="adjectivee"/>
    <form  suffix="half" tag="adjectiveno_e"/>
    <form  suffix="halve" tag="adjectivee"/>
  </table>
  <table name="adjective143" rads=".*(doodsble|we|ble)" fast="-">
    <!-- 3 members -->
    <form  suffix="ek" tag="adjectiveno_e"/>
    <form  suffix="ekst" tag="adjectivest"/>
    <form  suffix="ekste" tag="adjectiveste"/>
    <form  suffix="ke" tag="adjectivee"/>
    <form  suffix="ker" tag="adjectiveer"/>
    <form  suffix="kere" tag="adjectiveere"/>
  </table>
  <table name="adjective144" rads=".*(psychopat|myt|telepat)" fast="-">
    <!-- 3 members -->
    <form  suffix="hisch" tag="adjectiveno_e"/>
    <form  suffix="hische" tag="adjectivee"/>
    <form  suffix="isch" tag="adjectiveno_e"/>
    <form  suffix="ische" tag="adjectivee"/>
  </table>
  <table name="adjective145" rads=".*(se|heterose|homose)" fast="-">
    <!-- 3 members -->
    <form  suffix="ksueel" tag="adjectiveno_e"/>
    <form  suffix="ksuele" tag="adjectivee"/>
    <form  suffix="xueel" tag="adjectiveno_e"/>
    <form  suffix="xuele" tag="adjectivee"/>
  </table>
  <table name="adjective146" rads=".*(gedo|geno|geëcho)" fast="-">
    <!-- 3 members -->
    <form  suffix="od" tag="adjectivege_no_e"/>
    <form  suffix="de" tag="adjectivege_e"/>
  </table>
  <table name="adjective147" rads=".*(vro|slo|lo)" fast="-">
    <!-- 3 members -->
    <form  suffix="om" tag="adjectiveno_e"/>
    <form  suffix="me" tag="adjectivee"/>
    <form  suffix="mer" tag="adjectiveer"/>
    <form  suffix="mere" tag="adjectiveere"/>
    <form  suffix="omst" tag="adjectivest"/>
    <form  suffix="omste" tag="adjectiveste"/>
  </table>
  <table name="adjective148" rads=".*(tijdlo|roerlo|feillo)" fast="-">
    <!-- 3 members -->
    <form  suffix="os" tag="adjectiveno_e"/>
    <form  suffix="ze" tag="adjectivee"/>
    <form  suffix="zer" tag="adjectiveer"/>
    <form  suffix="zere" tag="adjectiveere"/>
  </table>
  <table name="adjective149" rads=".*(devo|overgro|blo)" fast="-">
    <!-- 3 members -->
    <form  suffix="ot" tag="adjectiveno_e"/>
    <form  suffix="otst" tag="adjectivest"/>
    <form  suffix="otste" tag="adjectiveste"/>
    <form  suffix="te" tag="adjectivee"/>
    <form  suffix="ter" tag="adjectiveer"/>
    <form  suffix="tere" tag="adjectiveere"/>
  </table>
  <table name="adjective150" rads=".*(acu|bru|resolu)" fast="-">
    <!-- 3 members -->
    <form  suffix="ut" tag="adjectiveno_e"/>
    <form  suffix="te" tag="adjectivee"/>
    <form  suffix="ter" tag="adjectiveer"/>
    <form  suffix="tere" tag="adjectiveere"/>
    <form  suffix="utst" tag="adjectivest"/>
    <form  suffix="utste" tag="adjectiveste"/>
  </table>
  <table name="als_adjective1" rads=".*(evenzeer|zomin|evenmin)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="als_adjectiveboth"/>
  </table>
  <table name="c1" rads=".*">
    <!-- 39 members -->
    <form  suffix="" tag="omplementizer"/>
  </table>
  <table name="c2" rads=".*(zat|beu|gewend|spuugzat|gewoon|waard)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="lause_np_adjective"/>
  </table>
  <table name="comp_determiner1" rads=".*(eenzelfde|diezelfde|dezelfde)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="comp_determinerde,als"/>
  </table>
  <table name="comp_determiner2" rads=".*(hetzelfde|ditzelfde|datzelfde)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="comp_determinerhet,als"/>
  </table>
  <table name="comp_determiner3" rads=".*(genoeg|voldoende|onvoldoende)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="comp_determinerwat,om"/>
  </table>
  <table name="complementizer1" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="complementizerroot"/>
  </table>
  <table name="complementizer2" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="complementizerinf"/>
  </table>
  <table name="complementizer3" rads=".*(ofschoon|hoewel|indien|alhoewel|waar|wanneer)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="complementizera"/>
    <form  suffix="" tag="complementizerpp"/>
  </table>
  <table name="complementizer4" rads=".*" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="complementizera"/>
    <form  suffix="" tag="complementizeradv"/>
    <form  suffix="" tag="complementizernp"/>
    <form  suffix="" tag="complementizerpp"/>
  </table>
  <table name="complementizer5" rads=".*(gelijk|voorzover|inzover)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="complementizerzoals"/>
  </table>
  <table name="conj1" rads=".*(alsmede|alsook|maar)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="conjmaar"/>
  </table>
  <table name="d1" rads=".*">
    <!-- 36 members -->
    <form  suffix="" tag="ir_adverb"/>
  </table>
  <table name="determiner1" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="determinerpron"/>
  </table>
  <table name="determiner2" rads=".*(genoeg|evenzoveel|voldoende|onvoldoende|evenveel)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="determinerwat,nwh,mod,pro,yparg"/>
  </table>
  <table name="determiner3" rads=".*(hetzelfde|ditzelfde|datzelfde)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="determinerhet,nwh,mod,pro,yparg"/>
  </table>
  <table name="determiner4" rads=".*(tal|allerlei|volop)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="determinerwat"/>
  </table>
  <table name="e1" rads=".*(overal|hier|nergens|ergens|daar)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="r_loc_adverb"/>
  </table>
  <table name="er_adverb1" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="er_adverbnaar"/>
  </table>
  <table name="er_adverb2" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="er_adverbtegen"/>
  </table>
  <table name="er_adverb3" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="er_adverbvan"/>
  </table>
  <table name="er_adverb4" rads=".*(hierachter|erachter|daarachteraan|erachteraan|hierachteraan|daarachter)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="er_adverbachter"/>
  </table>
  <table name="er_adverb5" rads=".*(erbovenuit|daarboven|erboven|daarbovenuit|hierbovenuit|hierboven)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="er_adverbboven"/>
  </table>
  <table name="er_adverb6" rads=".*(erdoor|hierdoorheen|erdoorheen|hierdoor|daardoor|daardoorheen)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="er_adverbdoor"/>
  </table>
  <table name="er_adverb7" rads=".*(ermee|hiermee|ermede|daarmede|hiermede|daarmee)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="er_adverbmet"/>
  </table>
  <table name="er_adverb8" rads=".*(erover|eroverheen|hierover|daaroverheen|daarover|hieroverheen)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="er_adverbover"/>
  </table>
  <table name="er_adverb9" rads=".*(vandaaruit|hiervanuit|ervanuit|vanhieruit|daarvanuit)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="er_adverbvanuit"/>
  </table>
  <table name="er_adverb10" rads=".*(daarlangs|hierlangs|erlangs|langsdaar)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="er_adverblangs"/>
  </table>
  <table name="er_adverb11" rads=".*(vandaaraf|daarvanaf|ervanaf|vanhieraf)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="er_adverbvanaf"/>
  </table>
  <table name="er_adverb12" rads=".*(eraan|hieraan|daaraan)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbaan"/>
  </table>
  <table name="er_adverb13" rads=".*(daarbij|erbij|hierbij)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbbij"/>
  </table>
  <table name="er_adverb14" rads=".*(daarbovenop|hierbovenop|erbovenop)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbbovenop"/>
  </table>
  <table name="er_adverb15" rads=".*(hierin|daarin|erin)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbin"/>
  </table>
  <table name="er_adverb16" rads=".*(hierna|erna|daarna)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbna"/>
  </table>
  <table name="er_adverb17" rads=".*(ernaast|daarnaast|hiernaast)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbnaast"/>
  </table>
  <table name="er_adverb18" rads=".*(erom|daarom|hierom)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbom"/>
  </table>
  <table name="er_adverb19" rads=".*(eromheen|hieromheen|daaromheen)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbom"/>
    <form  suffix="" tag="er_adverbomheen"/>
  </table>
  <table name="er_adverb20" rads=".*(eromtrent|daaromtrent|hieromtrent)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbomtrent"/>
  </table>
  <table name="er_adverb21" rads=".*(eronder|hieronder|daaronder)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbonder"/>
  </table>
  <table name="er_adverb22" rads=".*(erop|daarop|hierop)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbop"/>
  </table>
  <table name="er_adverb23" rads=".*(errond|daarrond|hierrond)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbrond"/>
  </table>
  <table name="er_adverb24" rads=".*(hiertegenin|daartegenin|ertegenin)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbtegen"/>
    <form  suffix="" tag="er_adverbtegenin"/>
  </table>
  <table name="er_adverb25" rads=".*(daartegenover|hiertegenover|ertegenover)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbtegenover"/>
  </table>
  <table name="er_adverb26" rads=".*(hiertoe|daartoe|ertoe)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbtot"/>
  </table>
  <table name="er_adverb27" rads=".*(hiertussen|ertussen|daartussen)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbtussen"/>
  </table>
  <table name="er_adverb28" rads=".*(daartussendoor|hiertussendoor|ertussendoor)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbtussendoor"/>
  </table>
  <table name="er_adverb29" rads=".*(hiertussenin|daartussenin|ertussenin)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbtussenin"/>
  </table>
  <table name="er_adverb30" rads=".*(daartussenuit|ertussenuit|hiertussenuit)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbtussenuit"/>
  </table>
  <table name="er_adverb31" rads=".*(daaruit|hieruit|eruit)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbuit"/>
  </table>
  <table name="er_adverb32" rads=".*(hieropuit|daaropuit|eropuit)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbuit_op"/>
  </table>
  <table name="er_adverb33" rads=".*(ervoor|daarvoor|hiervoor)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="er_adverbvoor"/>
  </table>
  <table name="i1" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="" tag="ets_noun"/>
  </table>
  <table name="i2" rads=".*(donders|te|heel)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="ntensifier"/>
  </table>
  <table name="l1" rads=".*">
    <!-- 78 members -->
    <form  suffix="" tag="oc_adverb"/>
  </table>
  <table name="left_conj1" rads=".*(en|achtereenvolgens|respectievelijk)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="left_conjen"/>
  </table>
  <table name="m1" rads=".*">
    <!-- 34 members -->
    <form  suffix="" tag="odal_adverb"/>
  </table>
  <table name="m2" rads=".*[ndstg]" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="od_postposition"/>
  </table>
  <table name="meas_mod_noun1" rads=".*">
    <!-- 56 members -->
    <form  suffix="" tag="meas_mod_nounboth,count,meas"/>
    <form  suffix="" tag="meas_mod_nounboth,count,meas,measure"/>
  </table>
  <table name="meas_mod_noun2" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="" tag="meas_mod_nounde,count,meas"/>
    <form  suffix="" tag="meas_mod_nounde,count,meas,measure"/>
    <form  suffix="s" tag="meas_mod_nounde,count,pl"/>
    <form  suffix="s" tag="meas_mod_nounde,count,pl,measure"/>
  </table>
  <table name="meas_mod_noun3" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="" tag="meas_mod_nounde,count,meas"/>
    <form  suffix="" tag="meas_mod_nounde,count,meas,measure"/>
  </table>
  <table name="meas_mod_noun4" rads=".*(triljoen|miljard|biljoen|miljoen|punt)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="meas_mod_nounboth,count,meas"/>
    <form  suffix="" tag="meas_mod_nounboth,count,meas,measure"/>
    <form  suffix="en" tag="meas_mod_nounboth,count,pl"/>
    <form  suffix="en" tag="meas_mod_nounboth,count,pl,measure"/>
  </table>
  <table name="meas_mod_noun5" rads=".*(kilo|peseta|cc|ecu)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="meas_mod_nounde,count,meas"/>
    <form  suffix="" tag="meas_mod_nounde,count,meas,measure"/>
    <form  suffix="'s" tag="meas_mod_nounde,count,pl"/>
    <form  suffix="'s" tag="meas_mod_nounde,count,pl,measure"/>
  </table>
  <table name="meas_mod_noun6" rads=".*(eindje|kwartje|dubbeltje)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="meas_mod_nounhet,count,sg"/>
    <form  suffix="" tag="meas_mod_nounhet,count,sg,measure"/>
    <form  suffix="s" tag="meas_mod_nounhet,count,pl"/>
    <form  suffix="s" tag="meas_mod_nounhet,count,pl,measure"/>
  </table>
  <table name="mod_noun1" rads=".*(ietsje|tikkeltje|pietsje)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="mod_nounhet,count,sg"/>
    <form  suffix="" tag="mod_nounhet,count,sg,measure"/>
  </table>
  <table name="modal_adverb1" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="" tag="modal_adverbnoun_prep"/>
  </table>
  <table name="modal_adverb2" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="modal_adverbadv_noun_prep"/>
  </table>
  <table name="modal_adverb3" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="modal_adverbprep"/>
  </table>
  <table name="modal_adverb4" rads=".*(geheel|gans|rond|alweer|wijlen)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="modal_adverbnoun"/>
  </table>
  <table name="modal_adverb5" rads=".*(meteen|vlak|even|onmiddellijk)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="modal_adverbcomp_prep"/>
  </table>
  <table name="modal_adverb6" rads=".*(telkens|steeds|altijd)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="modal_adverbcomp"/>
  </table>
  <table name="n1" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="" tag="p_adjective"/>
  </table>
  <table name="noun1" rads=".*">
    <!-- 2254 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="s" tag="nounde,count,pl"/>
  </table>
  <table name="noun2" rads=".*">
    <!-- 1953 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="en" tag="nounde,count,pl"/>
  </table>
  <table name="noun3" rads=".*">
    <!-- 1666 members -->
    <form  suffix="" tag="nounde,mass,sg"/>
  </table>
  <table name="noun4" rads=".*">
    <!-- 759 members -->
    <form  suffix="" tag="nounhet,mass,sg"/>
  </table>
  <table name="noun5" rads=".*">
    <!-- 410 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
  </table>
  <table name="noun6" rads=".*">
    <!-- 254 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
  </table>
  <table name="noun7" rads=".*">
    <!-- 195 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="'s" tag="nounde,count,pl"/>
  </table>
  <table name="noun8" rads=".*">
    <!-- 178 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,sbar"/>
    <form  suffix="en" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun9" rads=".*">
    <!-- 148 members -->
    <form  suffix="" tag="nounboth,mass,sg"/>
  </table>
  <table name="noun10" rads=".*">
    <!-- 147 members -->
    <form  suffix="" tag="nounde,count,sg"/>
  </table>
  <table name="noun11" rads=".*">
    <!-- 143 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="n" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl"/>
  </table>
  <table name="noun12" rads=".*">
    <!-- 139 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="sen" tag="nounde,count,pl"/>
  </table>
  <table name="noun13" rads=".*">
    <!-- 133 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="n" tag="nounde,count,pl"/>
  </table>
  <table name="noun14" rads=".*">
    <!-- 92 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl"/>
  </table>
  <table name="noun15" rads=".*">
    <!-- 88 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="ken" tag="nounde,count,pl"/>
  </table>
  <table name="noun16" rads=".*">
    <!-- 81 members -->
    <form  suffix="id" tag="nounde,count,sg"/>
    <form  suffix="den" tag="nounde,count,pl"/>
  </table>
  <table name="noun17" rads=".*">
    <!-- 80 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="len" tag="nounde,count,pl"/>
  </table>
  <table name="noun18" rads=".*">
    <!-- 70 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="ten" tag="nounde,count,pl"/>
  </table>
  <table name="noun19" rads=".*">
    <!-- 69 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="pen" tag="nounde,count,pl"/>
  </table>
  <table name="noun20" rads=".*">
    <!-- 67 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,sbar"/>
  </table>
  <table name="noun21" rads=".*">
    <!-- 66 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="ën" tag="nounde,count,pl"/>
  </table>
  <table name="noun22" rads=".*">
    <!-- 65 members -->
    <form  suffix="an" tag="nounde,count,sg"/>
    <form  suffix="nen" tag="nounde,count,pl"/>
  </table>
  <table name="noun23" rads=".*">
    <!-- 64 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun24" rads=".*">
    <!-- 62 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="nen" tag="nounde,count,pl"/>
  </table>
  <table name="noun25" rads=".*">
    <!-- 60 members -->
    <form  suffix="s" tag="nounhet,count,sg"/>
    <form  suffix="zen" tag="nounhet,count,pl"/>
  </table>
  <table name="noun26" rads=".*">
    <!-- 55 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
    <form  suffix="s" tag="nounboth,count,pl"/>
  </table>
  <table name="noun27" rads=".*">
    <!-- 54 members -->
    <form  suffix="at" tag="nounde,count,sg"/>
    <form  suffix="ten" tag="nounde,count,pl"/>
  </table>
  <table name="noun28" rads=".*">
    <!-- 53 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,sbar"/>
    <form  suffix="s" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun29" rads=".*">
    <!-- 53 members -->
    <form  suffix="s" tag="nounde,count,sg"/>
    <form  suffix="zen" tag="nounde,count,pl"/>
  </table>
  <table name="noun30" rads=".*">
    <!-- 51 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,sbar"/>
  </table>
  <table name="noun31" rads=".*">
    <!-- 50 members -->
    <form  suffix="al" tag="nounde,count,sg"/>
    <form  suffix="len" tag="nounde,count,pl"/>
  </table>
  <table name="noun32" rads=".*">
    <!-- 50 members -->
    <form  suffix="f" tag="nounde,count,sg"/>
    <form  suffix="ven" tag="nounde,count,pl"/>
  </table>
  <table name="noun33" rads=".*">
    <!-- 49 members -->
    <form  suffix="" tag="nounde,count,pl"/>
  </table>
  <table name="noun34" rads=".*">
    <!-- 49 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun35" rads=".*">
    <!-- 48 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="ken" tag="nounhet,count,pl"/>
  </table>
  <table name="noun36" rads=".*">
    <!-- 42 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun37" rads=".*">
    <!-- 42 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="ten" tag="nounhet,count,pl"/>
  </table>
  <table name="noun38" rads=".*">
    <!-- 39 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
    <form  suffix="en" tag="nounboth,count,pl"/>
  </table>
  <table name="noun39" rads=".*">
    <!-- 39 members -->
    <form  suffix="id" tag="nounde,count,sg"/>
    <form  suffix="den" tag="nounde,count,pl"/>
    <form  suffix="den" tag="nounde,count,pl,sbar"/>
    <form  suffix="den" tag="nounde,count,pl,vp"/>
    <form  suffix="id" tag="nounde,count,sg,sbar"/>
    <form  suffix="id" tag="nounde,count,sg,vp"/>
  </table>
  <table name="noun40" rads=".*">
    <!-- 38 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
  </table>
  <table name="noun41" rads=".*">
    <!-- 38 members -->
    <form  suffix="og" tag="nounde,count,sg"/>
    <form  suffix="gen" tag="nounde,count,pl"/>
  </table>
  <table name="noun42" rads=".*">
    <!-- 38 members -->
    <form  suffix="ot" tag="nounde,count,sg"/>
    <form  suffix="ten" tag="nounde,count,pl"/>
  </table>
  <table name="noun43" rads=".*">
    <!-- 38 members -->
    <form  suffix="ur" tag="nounde,count,sg"/>
    <form  suffix="ren" tag="nounde,count,pl"/>
  </table>
  <table name="noun44" rads=".*">
    <!-- 36 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
  </table>
  <table name="noun45" rads=".*">
    <!-- 36 members -->
    <form  suffix="ar" tag="nounde,count,sg"/>
    <form  suffix="ren" tag="nounde,count,pl"/>
  </table>
  <table name="noun46" rads=".*">
    <!-- 35 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,app_measure"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,app_measure"/>
  </table>
  <table name="noun47" rads=".*">
    <!-- 34 members -->
    <form  suffix="" tag="nounde,both,sg"/>
    <form  suffix="en" tag="nounde,count,pl"/>
  </table>
  <table name="noun48" rads=".*">
    <!-- 34 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="men" tag="nounde,count,pl"/>
  </table>
  <table name="noun49" rads=".*">
    <!-- 34 members -->
    <form  suffix="" tag="nounde,mass,sg"/>
    <form  suffix="" tag="nounde,mass,sg,pred_pp"/>
  </table>
  <table name="noun50" rads=".*">
    <!-- 32 members -->
    <form  suffix="op" tag="nounde,count,sg"/>
    <form  suffix="pen" tag="nounde,count,pl"/>
  </table>
  <table name="noun51" rads=".*" fast="-">
    <!-- 29 members -->
    <form  suffix="e" tag="nounde,count,sg"/>
    <form  suffix="es" tag="nounde,count,pl"/>
    <form  suffix="ën" tag="nounde,count,pl"/>
  </table>
  <table name="noun52" rads=".*" fast="-">
    <!-- 29 members -->
    <form  suffix="f" tag="nounhet,count,sg"/>
    <form  suffix="ven" tag="nounhet,count,pl"/>
  </table>
  <table name="noun53" rads=".*" fast="-">
    <!-- 28 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
  </table>
  <table name="noun54" rads=".*" fast="-">
    <!-- 28 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="len" tag="nounhet,count,pl"/>
  </table>
  <table name="noun55" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="" tag="nounhet,count,sg,vp"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,sbar"/>
    <form  suffix="en" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun56" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="um" tag="nounhet,count,sg"/>
    <form  suffix="a" tag="nounhet,count,pl"/>
    <form  suffix="ums" tag="nounhet,count,pl"/>
  </table>
  <table name="noun57" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="us" tag="nounde,count,sg"/>
    <form  suffix="i" tag="nounde,count,pl"/>
  </table>
  <table name="noun58" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="at" tag="nounhet,count,sg"/>
    <form  suffix="ten" tag="nounhet,count,pl"/>
  </table>
  <table name="noun59" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="ek" tag="nounde,count,sg"/>
    <form  suffix="ken" tag="nounde,count,pl"/>
  </table>
  <table name="noun60" rads=".*" fast="-">
    <!-- 24 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,sbar"/>
  </table>
  <table name="noun61" rads=".*" fast="-">
    <!-- 24 members -->
    <form  suffix="al" tag="nounhet,count,sg"/>
    <form  suffix="len" tag="nounhet,count,pl"/>
  </table>
  <table name="noun62" rads=".*" fast="-">
    <!-- 24 members -->
    <form  suffix="ar" tag="nounde,count,sg"/>
    <form  suffix="ars" tag="nounde,count,pl"/>
    <form  suffix="ren" tag="nounde,count,pl"/>
  </table>
  <table name="noun63" rads=".*" fast="-">
    <!-- 23 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="pen" tag="nounhet,count,pl"/>
  </table>
  <table name="noun64" rads=".*" fast="-">
    <!-- 23 members -->
    <form  suffix="ak" tag="nounde,count,sg"/>
    <form  suffix="ken" tag="nounde,count,pl"/>
  </table>
  <table name="noun65" rads=".*" fast="-">
    <!-- 23 members -->
    <form  suffix="es" tag="nounde,count,sg"/>
    <form  suffix="zen" tag="nounde,count,pl"/>
  </table>
  <table name="noun66" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,measure"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl,measure"/>
  </table>
  <table name="noun67" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="'s" tag="nounhet,count,pl"/>
  </table>
  <table name="noun68" rads=".*" fast="-">
    <!-- 22 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="sen" tag="nounhet,count,pl"/>
  </table>
  <table name="noun69" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun70" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="eren" tag="nounhet,count,pl"/>
  </table>
  <table name="noun71" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="el" tag="nounhet,count,sg"/>
    <form  suffix="len" tag="nounhet,count,pl"/>
  </table>
  <table name="noun72" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="en" tag="nounde,count,sg"/>
    <form  suffix="nen" tag="nounde,count,pl"/>
  </table>
  <table name="noun73" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="om" tag="nounde,count,sg"/>
    <form  suffix="men" tag="nounde,count,pl"/>
  </table>
  <table name="noun74" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,app_measure"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,app_measure"/>
  </table>
  <table name="noun75" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,subject_sbar"/>
    <form  suffix="" tag="nounde,count,sg,subject_vp"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,sbar"/>
    <form  suffix="en" tag="nounde,count,pl,subject_sbar"/>
    <form  suffix="en" tag="nounde,count,pl,subject_vp"/>
    <form  suffix="en" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun76" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="er" tag="nounde,count,sg"/>
    <form  suffix="ren" tag="nounde,count,pl"/>
  </table>
  <table name="noun77" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="on" tag="nounde,count,sg"/>
    <form  suffix="nen" tag="nounde,count,pl"/>
  </table>
  <table name="noun78" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="" tag="nounboth,count,pl"/>
  </table>
  <table name="noun79" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="ag" tag="nounde,count,sg"/>
    <form  suffix="gen" tag="nounde,count,pl"/>
  </table>
  <table name="noun80" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="id" tag="nounde,count,sg"/>
    <form  suffix="den" tag="nounde,count,pl"/>
    <form  suffix="den" tag="nounde,count,pl,vp"/>
    <form  suffix="id" tag="nounde,count,sg,vp"/>
  </table>
  <table name="noun81" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="ur" tag="nounhet,count,sg"/>
    <form  suffix="ren" tag="nounhet,count,pl"/>
  </table>
  <table name="noun82" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="" tag="nounde,count,both"/>
  </table>
  <table name="noun83" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,measure"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,measure"/>
  </table>
  <table name="noun84" rads=".*" fast="-">
    <!-- 16 members -->
    <form  suffix="et" tag="nounde,count,sg"/>
    <form  suffix="ten" tag="nounde,count,pl"/>
  </table>
  <table name="noun85" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="n" tag="nounde,count,pl"/>
    <form  suffix="n" tag="nounde,count,pl,sbar"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,sbar"/>
  </table>
  <table name="noun86" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="ol" tag="nounde,count,sg"/>
    <form  suffix="len" tag="nounde,count,pl"/>
  </table>
  <table name="noun87" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
    <form  suffix="'s" tag="nounboth,count,pl"/>
  </table>
  <table name="noun88" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
  </table>
  <table name="noun89" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="gen" tag="nounde,count,pl"/>
  </table>
  <table name="noun90" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="man" tag="nounde,count,sg"/>
    <form  suffix="lieden" tag="nounde,count,pl"/>
    <form  suffix="lui" tag="nounde,count,pl"/>
  </table>
  <table name="noun91" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,app_measure"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,app_measure"/>
  </table>
  <table name="noun92" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="ar" tag="nounhet,count,sg"/>
    <form  suffix="ren" tag="nounhet,count,pl"/>
  </table>
  <table name="noun93" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="id" tag="nounde,count,sg"/>
    <form  suffix="den" tag="nounde,count,pl"/>
    <form  suffix="den" tag="nounde,count,pl,sbar"/>
    <form  suffix="id" tag="nounde,count,sg,sbar"/>
  </table>
  <table name="noun94" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="um" tag="nounhet,count,sg"/>
    <form  suffix="a" tag="nounhet,count,pl"/>
  </table>
  <table name="noun95" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,pred_pp"/>
    <form  suffix="en" tag="nounde,count,pl"/>
  </table>
  <table name="noun96" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="n" tag="nounhet,count,pl"/>
  </table>
  <table name="noun97" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="n" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
  </table>
  <table name="noun98" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="as" tag="nounde,count,sg"/>
    <form  suffix="zen" tag="nounde,count,pl"/>
  </table>
  <table name="noun99" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="ken" tag="nounde,count,pl"/>
    <form  suffix="ken" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun100" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="es" tag="nounde,count,pl"/>
  </table>
  <table name="noun101" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,vp"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun102" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="ad" tag="nounde,count,sg"/>
    <form  suffix="den" tag="nounde,count,pl"/>
  </table>
  <table name="noun103" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="am" tag="nounde,count,sg"/>
    <form  suffix="men" tag="nounde,count,pl"/>
  </table>
  <table name="noun104" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,measure"/>
    <form  suffix="en" tag="nounde,count,pl,sbar"/>
  </table>
  <table name="noun105" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="ren" tag="nounde,count,pl"/>
  </table>
  <table name="noun106" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl,sbar"/>
  </table>
  <table name="noun107" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="ad" tag="nounde,count,sg"/>
    <form  suffix="eden" tag="nounde,count,pl"/>
  </table>
  <table name="noun108" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="n" tag="nounde,count,pl"/>
    <form  suffix="n" tag="nounde,count,pl,measure"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun109" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,np_app_measure"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,np_app_measure"/>
  </table>
  <table name="noun110" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="n" tag="nounde,count,pl"/>
    <form  suffix="n" tag="nounde,count,pl,sbar"/>
    <form  suffix="n" tag="nounde,count,pl,vp"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,sbar"/>
    <form  suffix="s" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun111" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="" tag="nounhet,both,sg"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
  </table>
  <table name="noun112" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="af" tag="nounde,count,sg"/>
    <form  suffix="ven" tag="nounde,count,pl"/>
  </table>
  <table name="noun113" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="ctie" tag="nounde,count,sg"/>
    <form  suffix="cties" tag="nounde,count,pl"/>
    <form  suffix="ktie" tag="nounde,count,sg"/>
    <form  suffix="kties" tag="nounde,count,pl"/>
  </table>
  <table name="noun114" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="om" tag="nounhet,count,sg"/>
    <form  suffix="men" tag="nounhet,count,pl"/>
  </table>
  <table name="noun115" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="or" tag="nounhet,count,sg"/>
    <form  suffix="ren" tag="nounhet,count,pl"/>
  </table>
  <table name="noun116" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="sen" tag="nounde,count,pl"/>
    <form  suffix="sen" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun117" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,subject_sbar"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,sbar"/>
    <form  suffix="en" tag="nounde,count,pl,subject_sbar"/>
    <form  suffix="en" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun118" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="'s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl"/>
  </table>
  <table name="noun119" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="nounde,mass,sg"/>
    <form  suffix="" tag="nounde,mass,sg,subject_sbar"/>
    <form  suffix="" tag="nounde,mass,sg,subject_vp"/>
  </table>
  <table name="noun120" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="" tag="nounhet,count,sg,subject_sbar"/>
    <form  suffix="" tag="nounhet,count,sg,subject_vp"/>
    <form  suffix="" tag="nounhet,count,sg,vp"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,sbar"/>
    <form  suffix="en" tag="nounhet,count,pl,subject_sbar"/>
    <form  suffix="en" tag="nounhet,count,pl,subject_vp"/>
    <form  suffix="en" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun121" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="nounhet,mass,sg"/>
    <form  suffix="" tag="nounhet,mass,sg,pred_pp"/>
  </table>
  <table name="noun122" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="el" tag="nounde,count,sg"/>
    <form  suffix="len" tag="nounde,count,pl"/>
  </table>
  <table name="noun123" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
    <form  suffix="sen" tag="nounboth,count,pl"/>
  </table>
  <table name="noun124" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,both,sg"/>
    <form  suffix="s" tag="nounde,count,pl"/>
  </table>
  <table name="noun125" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,measure"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun126" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="len" tag="nounde,count,pl"/>
    <form  suffix="len" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun127" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="men" tag="nounde,count,pl"/>
    <form  suffix="men" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun128" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="pen" tag="nounde,count,pl"/>
    <form  suffix="pen" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun129" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="n" tag="nounde,count,pl"/>
    <form  suffix="n" tag="nounde,count,pl,sbar"/>
  </table>
  <table name="noun130" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="n" tag="nounde,count,pl"/>
    <form  suffix="n" tag="nounde,count,pl,vp"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun131" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="fen" tag="nounde,count,pl"/>
  </table>
  <table name="noun132" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,mass,sg"/>
    <form  suffix="" tag="nounde,mass,sg,sbar"/>
  </table>
  <table name="noun133" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,mass,sg"/>
    <form  suffix="" tag="nounde,mass,sg,sbar"/>
    <form  suffix="" tag="nounde,mass,sg,vp"/>
  </table>
  <table name="noun134" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounde,mass,sg"/>
    <form  suffix="" tag="nounde,mass,sg,vp"/>
  </table>
  <table name="noun135" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,measure"/>
    <form  suffix="len" tag="nounhet,count,pl"/>
    <form  suffix="len" tag="nounhet,count,pl,measure"/>
  </table>
  <table name="noun136" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,pred_pp"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
  </table>
  <table name="noun137" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="" tag="nounhet,count,sg,vp"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl,sbar"/>
    <form  suffix="s" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun138" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,sbar"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl,sbar"/>
  </table>
  <table name="noun139" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="am" tag="nounhet,count,sg"/>
    <form  suffix="men" tag="nounhet,count,pl"/>
  </table>
  <table name="noun140" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ap" tag="nounde,count,sg"/>
    <form  suffix="pen" tag="nounde,count,pl"/>
  </table>
  <table name="noun141" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ep" tag="nounde,count,sg"/>
    <form  suffix="pen" tag="nounde,count,pl"/>
  </table>
  <table name="noun142" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="er" tag="nounhet,count,sg"/>
    <form  suffix="ren" tag="nounhet,count,pl"/>
  </table>
  <table name="noun143" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="f" tag="nounboth,count,sg"/>
    <form  suffix="ven" tag="nounboth,count,pl"/>
  </table>
  <table name="noun144" rads=".*(revers|capita|oost-west|noord-zuid|hare|mijne)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="nounboth,count,both"/>
  </table>
  <table name="noun145" rads=".*(kavel|kluwen|koppel|sample|bussel|shot)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
    <form  suffix="" tag="nounboth,count,sg,measure"/>
    <form  suffix="s" tag="nounboth,count,pl"/>
    <form  suffix="s" tag="nounboth,count,pl,measure"/>
  </table>
  <table name="noun146" rads=".*(blunder|concessie|remedie|kick|prestatie|rage)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,subject_sbar"/>
    <form  suffix="" tag="nounde,count,sg,subject_vp"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,subject_sbar"/>
    <form  suffix="s" tag="nounde,count,pl,subject_vp"/>
  </table>
  <table name="noun147" rads=".*(winst|ramp|schuld|verrassing|verschraling|belediging)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,subject_sbar"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,subject_sbar"/>
  </table>
  <table name="noun148" rads=".*(horizon|ballon|bon|bidon|pion|baron)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="nen" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl"/>
  </table>
  <table name="noun149" rads=".*(tap|strip|stop|slip|flip|tussenstop)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="pen" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl"/>
  </table>
  <table name="noun150" rads=".*(pee|parodie|fotografie|galerie|oranjerie|harmonie)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="ën" tag="nounde,count,pl"/>
  </table>
  <table name="noun151" rads=".*(zegen|pest|troost|kwetsbaarheid|werkelijkheid|tragiek)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="nounde,mass,sg"/>
    <form  suffix="" tag="nounde,mass,sg,subject_sbar"/>
  </table>
  <table name="noun152" rads=".*(achtervoegsel|item|misbaksel|voorvoegsel|college|cijfer)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,app_measure"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl,app_measure"/>
  </table>
  <table name="noun153" rads=".*(feit|gevolg|houvast|geheim|gerucht|wonder)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="" tag="nounhet,count,sg,subject_sbar"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,sbar"/>
    <form  suffix="en" tag="nounhet,count,pl,subject_sbar"/>
  </table>
  <table name="noun154" rads=".*(camera|hoofd|brandweer|doel|vertrouwens|wetenschaps)" fast="-">
    <!-- 6 members -->
    <form  suffix="man" tag="nounde,count,sg"/>
    <form  suffix="lieden" tag="nounde,count,pl"/>
    <form  suffix="mannen" tag="nounde,count,pl"/>
  </table>
  <table name="noun155" rads=".*(schoenendo|abriko|matro|tussenpo|virtuo|frambo)" fast="-">
    <!-- 6 members -->
    <form  suffix="os" tag="nounde,count,sg"/>
    <form  suffix="zen" tag="nounde,count,pl"/>
  </table>
  <table name="noun156" rads=".*(foren|sau|glan|fron|poe|spon)" fast="-">
    <!-- 6 members -->
    <form  suffix="s" tag="nounde,count,sg"/>
    <form  suffix="sen" tag="nounde,count,pl"/>
    <form  suffix="zen" tag="nounde,count,pl"/>
  </table>
  <table name="noun157" rads=".*(kwaliteit|rekening|last|beschikking|tijd)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,pred_pp"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,sbar"/>
    <form  suffix="en" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun158" rads=".*(illusie|dooddoener|leugen|sanctie|optie)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,subject_sbar"/>
    <form  suffix="" tag="nounde,count,sg,subject_vp"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,sbar"/>
    <form  suffix="s" tag="nounde,count,pl,subject_sbar"/>
    <form  suffix="s" tag="nounde,count,pl,subject_vp"/>
    <form  suffix="s" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun159" rads=".*(prikkel|stunt|grondregel|maatregel|voorzorgsmaatregel)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,sbar"/>
    <form  suffix="en" tag="nounde,count,pl,vp"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,sbar"/>
    <form  suffix="s" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun160" rads=".*(filosofie|theorie|ideologie|profetie|analogie)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="ën" tag="nounde,count,pl"/>
    <form  suffix="ën" tag="nounde,count,pl,sbar"/>
  </table>
  <table name="noun161" rads=".*(design|Tamil|nylon|mountainbike|koper)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounhet,mass,sg"/>
    <form  suffix="s" tag="nounde,count,pl"/>
  </table>
  <table name="noun162" rads=".*(cross|miss|speech|match|lunch)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="es" tag="nounde,count,pl"/>
  </table>
  <table name="noun163" rads=".*(diagram|heiligdom|vorstendom|cryptogram|bisdom)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="men" tag="nounhet,count,pl"/>
  </table>
  <table name="noun164" rads=".*(biogra|telegra|lexicogra|choreogra|fotogra)" fast="-">
    <!-- 5 members -->
    <form  suffix="af" tag="nounde,count,sg"/>
    <form  suffix="fen" tag="nounde,count,pl"/>
  </table>
  <table name="noun165" rads=".*(spira|radica|gaspeda|ova|peda)" fast="-">
    <!-- 5 members -->
    <form  suffix="al" tag="nounboth,count,sg"/>
    <form  suffix="len" tag="nounboth,count,pl"/>
  </table>
  <table name="noun166" rads=".*(beperkthe|betrekkelijkhe|bekendhe|overgevoelighe|onbekendhe)" fast="-">
    <!-- 5 members -->
    <form  suffix="id" tag="nounde,both,sg"/>
    <form  suffix="den" tag="nounde,count,pl"/>
  </table>
  <table name="noun167" rads=".*(krijgs|bewinds|raads|edel|leids)" fast="-">
    <!-- 5 members -->
    <form  suffix="man" tag="nounde,count,sg"/>
    <form  suffix="lieden" tag="nounde,count,pl"/>
  </table>
  <table name="noun168" rads=".*(temp|concert|port|intermezz|sald)" fast="-">
    <!-- 5 members -->
    <form  suffix="o" tag="nounhet,count,sg"/>
    <form  suffix="i" tag="nounhet,count,pl"/>
    <form  suffix="o's" tag="nounhet,count,pl"/>
  </table>
  <table name="noun169" rads=".*(telefo|grammofo|kleinzo|microfo|zo)" fast="-">
    <!-- 5 members -->
    <form  suffix="on" tag="nounde,count,sg"/>
    <form  suffix="nen" tag="nounde,count,pl"/>
    <form  suffix="ons" tag="nounde,count,pl"/>
  </table>
  <table name="noun170" rads=".*(No|domo|bo|stuwado|Mo)" fast="-">
    <!-- 5 members -->
    <form  suffix="or" tag="nounde,count,sg"/>
    <form  suffix="ren" tag="nounde,count,pl"/>
  </table>
  <table name="noun171" rads=".*(onbenul|korfbal|honkbal|atol)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
    <form  suffix="len" tag="nounboth,count,pl"/>
  </table>
  <table name="noun172" rads=".*(hunne|uwe|onze|gilde)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
    <form  suffix="n" tag="nounboth,count,pl"/>
  </table>
  <table name="noun173" rads=".*(ridderschap|buurtschap|woiwodschap|nalatenschap)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
    <form  suffix="pen" tag="nounboth,count,pl"/>
  </table>
  <table name="noun174" rads=".*(honderdduizenden|duizenden|tienduizenden|honderden)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounde,count,pl"/>
    <form  suffix="" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun175" rads=".*(brandstof|kleurstof|vloeistof|grondstof)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,app_measure"/>
    <form  suffix="fen" tag="nounde,count,pl"/>
    <form  suffix="fen" tag="nounde,count,pl,app_measure"/>
  </table>
  <table name="noun176" rads=".*(onderorde|aangifte|suborde|ziekte)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,app_measure"/>
    <form  suffix="n" tag="nounde,count,pl"/>
    <form  suffix="n" tag="nounde,count,pl,app_measure"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,app_measure"/>
  </table>
  <table name="noun177" rads=".*(trits|zweem|bulk|rist)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
  </table>
  <table name="noun178" rads=".*(constante|schare|laatstgenoemde|roede)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="n" tag="nounde,count,pl"/>
    <form  suffix="n" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun179" rads=".*(klacht|wijziging|redenering|beschuldiging)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,subject_sbar"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,sbar"/>
    <form  suffix="en" tag="nounde,count,pl,subject_sbar"/>
  </table>
  <table name="noun180" rads=".*(levenswijze|zede|grondgedachte|levensbehoefte)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="n" tag="nounde,count,pl"/>
    <form  suffix="n" tag="nounde,count,pl,sbar"/>
    <form  suffix="n" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun181" rads=".*(uitwas|ergernis|gelijkenis|geschiedenis)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="sen" tag="nounde,count,pl"/>
    <form  suffix="sen" tag="nounde,count,pl,sbar"/>
  </table>
  <table name="noun182" rads=".*(voorzetsel|rendez-vous|NK-viertallen|chassis)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounhet,count,both"/>
  </table>
  <table name="noun183" rads=".*(ochtenduren|hulpgoederen|argusogen|dovemansoren)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounhet,count,pl"/>
  </table>
  <table name="noun184" rads=".*(ontwerp|steunpunt|beroep|voorbeeld)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,app_measure"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="" tag="nounhet,count,sg,vp"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,app_measure"/>
    <form  suffix="en" tag="nounhet,count,pl,sbar"/>
    <form  suffix="en" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun185" rads=".*(bouquet|kwartet|pakket|kwintet)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,measure"/>
    <form  suffix="ten" tag="nounhet,count,pl"/>
    <form  suffix="ten" tag="nounhet,count,pl,measure"/>
  </table>
  <table name="noun186" rads=".*(prijsniveau|kaliber|opleidingsniveau|niveau)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,pred_pp"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
  </table>
  <table name="noun187" rads=".*(verzinsel|grondbeginsel|uitvloeisel|mirakel)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="" tag="nounhet,count,sg,vp"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,sbar"/>
    <form  suffix="en" tag="nounhet,count,pl,vp"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl,sbar"/>
    <form  suffix="s" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun188" rads=".*(rif|middenrif|tegengif|gif)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="fen" tag="nounhet,count,pl"/>
  </table>
  <table name="noun189" rads=".*(ven|gezin|huisgezin|gastgezin)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="nen" tag="nounhet,count,pl"/>
  </table>
  <table name="noun190" rads=".*(wanda|ra|welda|misda)" fast="-">
    <!-- 4 members -->
    <form  suffix="ad" tag="nounde,count,sg"/>
    <form  suffix="ad" tag="nounde,count,sg,sbar"/>
    <form  suffix="ad" tag="nounde,count,sg,vp"/>
    <form  suffix="den" tag="nounde,count,pl"/>
    <form  suffix="den" tag="nounde,count,pl,sbar"/>
    <form  suffix="den" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun191" rads=".*(kwa|siera|za|gewa)" fast="-">
    <!-- 4 members -->
    <form  suffix="ad" tag="nounhet,count,sg"/>
    <form  suffix="den" tag="nounhet,count,pl"/>
  </table>
  <table name="noun192" rads=".*(vierma|vijfma|tweema|driema)" fast="-">
    <!-- 4 members -->
    <form  suffix="al" tag="nounboth,count,bare_meas"/>
    <form  suffix="al" tag="nounboth,count,bare_meas,measure"/>
    <form  suffix="al" tag="nounboth,count,bare_meas,sbar"/>
    <form  suffix="len" tag="nounboth,count,pl"/>
    <form  suffix="len" tag="nounboth,count,pl,measure"/>
    <form  suffix="len" tag="nounboth,count,pl,sbar"/>
  </table>
  <table name="noun193" rads=".*(trakta|postula|tracta|manda)" fast="-">
    <!-- 4 members -->
    <form  suffix="at" tag="nounhet,count,sg"/>
    <form  suffix="at" tag="nounhet,count,sg,sbar"/>
    <form  suffix="at" tag="nounhet,count,sg,vp"/>
    <form  suffix="ten" tag="nounhet,count,pl"/>
    <form  suffix="ten" tag="nounhet,count,pl,sbar"/>
    <form  suffix="ten" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun194" rads=".*(consula|secretaria|directora|inspectora)" fast="-">
    <!-- 4 members -->
    <form  suffix="at-generaal" tag="nounhet,count,sg"/>
    <form  suffix="ten-generaal" tag="nounhet,count,pl"/>
  </table>
  <table name="noun195" rads=".*(obje|inse|diale|traje)" fast="-">
    <!-- 4 members -->
    <form  suffix="ct" tag="nounhet,count,sg"/>
    <form  suffix="cten" tag="nounhet,count,pl"/>
    <form  suffix="kt" tag="nounhet,count,sg"/>
    <form  suffix="kten" tag="nounhet,count,pl"/>
  </table>
  <table name="noun196" rads=".*(industrializati|industrialisati|bacteri|pori)" fast="-">
    <!-- 4 members -->
    <form  suffix="e" tag="nounde,count,sg"/>
    <form  suffix="ën" tag="nounde,count,pl"/>
  </table>
  <table name="noun197" rads=".*(ne|te|ze|wre)" fast="-">
    <!-- 4 members -->
    <form  suffix="ef" tag="nounde,count,sg"/>
    <form  suffix="ven" tag="nounde,count,pl"/>
  </table>
  <table name="noun198" rads=".*(ecze|syste|emble|extre)" fast="-">
    <!-- 4 members -->
    <form  suffix="em" tag="nounhet,count,sg"/>
    <form  suffix="men" tag="nounhet,count,pl"/>
  </table>
  <table name="noun199" rads=".*(Vietname|Albane|Portuge|Senegale)" fast="-">
    <!-- 4 members -->
    <form  suffix="es" tag="nounde,count,sg"/>
    <form  suffix="es" tag="nounhet,mass,sg"/>
    <form  suffix="zen" tag="nounde,count,pl"/>
  </table>
  <table name="noun200" rads=".*(tu|rekru|bru|recru)" fast="-">
    <!-- 4 members -->
    <form  suffix="ut" tag="nounde,count,sg"/>
    <form  suffix="ten" tag="nounde,count,pl"/>
  </table>
  <table name="noun201" rads=".*(statu|dispu|salu|rijksinstitu)" fast="-">
    <!-- 4 members -->
    <form  suffix="ut" tag="nounhet,count,sg"/>
    <form  suffix="ten" tag="nounhet,count,pl"/>
  </table>
  <table name="noun202" rads=".*(boeket|krat|tablet)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
    <form  suffix="" tag="nounboth,count,sg,measure"/>
    <form  suffix="ten" tag="nounboth,count,pl"/>
    <form  suffix="ten" tag="nounboth,count,pl,measure"/>
  </table>
  <table name="noun203" rads=".*(weeklang|tijdlang|eenmaal)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounboth,count,sg"/>
    <form  suffix="" tag="nounboth,count,sg,sbar"/>
  </table>
  <table name="noun204" rads=".*(schoppen|harten|Thai)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,both"/>
    <form  suffix="s" tag="nounde,count,pl"/>
  </table>
  <table name="noun205" rads=".*(titel|ondertitel|workshop)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,app_measure"/>
    <form  suffix="" tag="nounde,count,sg,np_app_measure"/>
    <form  suffix="" tag="nounde,count,sg,start_app_measure"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,app_measure"/>
    <form  suffix="s" tag="nounde,count,pl,np_app_measure"/>
    <form  suffix="s" tag="nounde,count,pl,start_app_measure"/>
  </table>
  <table name="noun206" rads=".*(lichting|hand|verhouding)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="" tag="nounde,count,sg,pred_pp"/>
    <form  suffix="en" tag="nounde,count,pl"/>
    <form  suffix="en" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun207" rads=".*(knot|spat|dot)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,measure"/>
    <form  suffix="ten" tag="nounde,count,pl"/>
    <form  suffix="ten" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun208" rads=".*(documentaire|camping|manege)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,np_app_measure"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,np_app_measure"/>
  </table>
  <table name="noun209" rads=".*(allure|controle|perfectie)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,pred_pp"/>
    <form  suffix="s" tag="nounde,count,pl"/>
  </table>
  <table name="noun210" rads=".*(consequentie|opsteker|vuistregel)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,subject_sbar"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,sbar"/>
    <form  suffix="s" tag="nounde,count,pl,subject_sbar"/>
  </table>
  <table name="noun211" rads=".*(inval|zeepbel|uitval)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="len" tag="nounde,count,pl"/>
    <form  suffix="len" tag="nounde,count,pl,sbar"/>
    <form  suffix="len" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun212" rads=".*(verbintenis|bekentenis|belijdenis)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="" tag="nounde,count,sg,vp"/>
    <form  suffix="sen" tag="nounde,count,pl"/>
    <form  suffix="sen" tag="nounde,count,pl,sbar"/>
    <form  suffix="sen" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun213" rads=".*(mop|weddenschap|wetenschap)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,sbar"/>
    <form  suffix="pen" tag="nounde,count,pl"/>
    <form  suffix="pen" tag="nounde,count,pl,sbar"/>
  </table>
  <table name="noun214" rads=".*(complicatie|versie|clou)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg,subject_sbar"/>
    <form  suffix="s" tag="nounde,count,pl"/>
    <form  suffix="s" tag="nounde,count,pl,subject_sbar"/>
  </table>
  <table name="noun215" rads=".*(voetbal|basketbal|volleybal)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounhet,mass,sg"/>
    <form  suffix="len" tag="nounde,count,pl"/>
  </table>
  <table name="noun216" rads=".*(krab|rib|krib)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="ben" tag="nounde,count,pl"/>
  </table>
  <table name="noun217" rads=".*(gok|trots|consensus)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,mass,sg"/>
    <form  suffix="" tag="nounde,mass,sg,sbar"/>
    <form  suffix="" tag="nounde,mass,sg,subject_sbar"/>
  </table>
  <table name="noun218" rads=".*(samenhang|opwaardering|griep)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounde,mass,sg"/>
    <form  suffix="en" tag="nounde,count,pl"/>
  </table>
  <table name="noun219" rads=".*(wereldkampioenschap|kampioenschap|bedrijfsschap)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,app_measure"/>
    <form  suffix="pen" tag="nounhet,count,pl"/>
    <form  suffix="pen" tag="nounhet,count,pl,app_measure"/>
  </table>
  <table name="noun220" rads=".*(diploma|salvo|trio)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,measure"/>
    <form  suffix="'s" tag="nounhet,count,pl"/>
    <form  suffix="'s" tag="nounhet,count,pl,measure"/>
  </table>
  <table name="noun221" rads=".*(vak|pak|hok)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,measure"/>
    <form  suffix="ken" tag="nounhet,count,pl"/>
    <form  suffix="ken" tag="nounhet,count,pl,measure"/>
  </table>
  <table name="noun222" rads=".*(sterrenbeeld|oord|park)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,np_app_measure"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,np_app_measure"/>
  </table>
  <table name="noun223" rads=".*(album|restaurant|hotel)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,np_app_measure"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl,np_app_measure"/>
  </table>
  <table name="noun224" rads=".*(vooruitzicht|verwijt|tegenargument)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="" tag="nounhet,count,sg,subject_sbar"/>
    <form  suffix="" tag="nounhet,count,sg,vp"/>
    <form  suffix="en" tag="nounhet,count,pl"/>
    <form  suffix="en" tag="nounhet,count,pl,sbar"/>
    <form  suffix="en" tag="nounhet,count,pl,subject_sbar"/>
    <form  suffix="en" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun225" rads=".*(kommando|risico|scenario)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="" tag="nounhet,count,sg,vp"/>
    <form  suffix="'s" tag="nounhet,count,pl"/>
    <form  suffix="'s" tag="nounhet,count,pl,sbar"/>
    <form  suffix="'s" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun226" rads=".*(meningsverschil|noodgeval|lotgeval)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="" tag="nounhet,count,sg,vp"/>
    <form  suffix="len" tag="nounhet,count,pl"/>
    <form  suffix="len" tag="nounhet,count,pl,sbar"/>
    <form  suffix="len" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun227" rads=".*(imago|hoofdthema|alibi)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="'s" tag="nounhet,count,pl"/>
    <form  suffix="'s" tag="nounhet,count,pl,sbar"/>
  </table>
  <table name="noun228" rads=".*(axioma|dogma|paradigma)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,sbar"/>
    <form  suffix="'s" tag="nounhet,count,pl"/>
    <form  suffix="'s" tag="nounhet,count,pl,sbar"/>
    <form  suffix="ta" tag="nounhet,count,pl"/>
    <form  suffix="ta" tag="nounhet,count,pl,sbar"/>
  </table>
  <table name="noun229" rads=".*(genoegen|loontje|spektakel)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="" tag="nounhet,count,sg,vp"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl,vp"/>
  </table>
  <table name="noun230" rads=".*(decolleté|communiqué|extra)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="'s" tag="nounhet,count,pl"/>
    <form  suffix="s" tag="nounhet,count,pl"/>
  </table>
  <table name="noun231" rads=".*(prisma|schisma|schema)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="'s" tag="nounhet,count,pl"/>
    <form  suffix="ta" tag="nounhet,count,pl"/>
  </table>
  <table name="noun232" rads=".*(bed|getij|wad)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,count,sg"/>
    <form  suffix="den" tag="nounhet,count,pl"/>
  </table>
  <table name="noun233" rads=".*(gedoe|benul|geluk)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="nounhet,mass,sg"/>
    <form  suffix="" tag="nounhet,mass,sg,sbar"/>
    <form  suffix="" tag="nounhet,mass,sg,vp"/>
  </table>
  <table name="noun234" rads=".*(procureur|gouverneur|goeverneur)" fast="-">
    <!-- 3 members -->
    <form  suffix="-generaal" tag="nounde,count,sg"/>
    <form  suffix="s-generaal" tag="nounde,count,pl"/>
  </table>
  <table name="noun235" rads=".*(concla|octa|okta)" fast="-">
    <!-- 3 members -->
    <form  suffix="af" tag="nounhet,count,sg"/>
    <form  suffix="ven" tag="nounhet,count,pl"/>
  </table>
  <table name="noun236" rads=".*(scha|stra|ba)" fast="-">
    <!-- 3 members -->
    <form  suffix="al" tag="nounde,count,sg"/>
    <form  suffix="al" tag="nounde,count,sg,measure"/>
    <form  suffix="len" tag="nounde,count,pl"/>
    <form  suffix="len" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun237" rads=".*(uitha|mijlpa|omha)" fast="-">
    <!-- 3 members -->
    <form  suffix="al" tag="nounde,count,sg"/>
    <form  suffix="al" tag="nounde,count,sg,sbar"/>
    <form  suffix="al" tag="nounde,count,sg,vp"/>
    <form  suffix="len" tag="nounde,count,pl"/>
    <form  suffix="len" tag="nounde,count,pl,sbar"/>
    <form  suffix="len" tag="nounde,count,pl,vp"/>
  </table>
  <table name="noun238" rads=".*(resulta|eindresulta|onderzoeksresulta)" fast="-">
    <!-- 3 members -->
    <form  suffix="at" tag="nounhet,count,sg"/>
    <form  suffix="at" tag="nounhet,count,sg,sbar"/>
    <form  suffix="at" tag="nounhet,count,sg,subject_sbar"/>
    <form  suffix="ten" tag="nounhet,count,pl"/>
    <form  suffix="ten" tag="nounhet,count,pl,sbar"/>
    <form  suffix="ten" tag="nounhet,count,pl,subject_sbar"/>
  </table>
  <table name="noun239" rads=".*(distri|proje|produ)" fast="-">
    <!-- 3 members -->
    <form  suffix="ct" tag="nounhet,count,sg"/>
    <form  suffix="ct" tag="nounhet,count,sg,app_measure"/>
    <form  suffix="cten" tag="nounhet,count,pl"/>
    <form  suffix="cten" tag="nounhet,count,pl,app_measure"/>
    <form  suffix="kt" tag="nounhet,count,sg"/>
    <form  suffix="kt" tag="nounhet,count,sg,app_measure"/>
    <form  suffix="kten" tag="nounhet,count,pl"/>
    <form  suffix="kten" tag="nounhet,count,pl,app_measure"/>
  </table>
  <table name="noun240" rads=".*(produ|radioa|obje)" fast="-">
    <!-- 3 members -->
    <form  suffix="ctiviteit" tag="nounde,mass,sg"/>
    <form  suffix="ktiviteit" tag="nounde,mass,sg"/>
  </table>
  <table name="noun241" rads=".*(verwant|schred|kliënt)" fast="-">
    <!-- 3 members -->
    <form  suffix="e" tag="nounde,count,sg"/>
    <form  suffix="" tag="nounde,count,sg"/>
    <form  suffix="en" tag="nounde,count,pl"/>
  </table>
  <table name="noun242" rads=".*(inspecti|studi|religi)" fast="-">
    <!-- 3 members -->
    <form  suffix="e" tag="nounde,count,sg"/>
    <form  suffix="e" tag="nounde,count,sg,app_measure"/>
    <form  suffix="es" tag="nounde,count,pl"/>
    <form  suffix="es" tag="nounde,count,pl,app_measure"/>
    <form  suffix="ën" tag="nounde,count,pl"/>
    <form  suffix="ën" tag="nounde,count,pl,app_measure"/>
  </table>
  <table name="noun243" rads=".*(koloni|divisi|seri)" fast="-">
    <!-- 3 members -->
    <form  suffix="e" tag="nounde,count,sg"/>
    <form  suffix="e" tag="nounde,count,sg,measure"/>
    <form  suffix="es" tag="nounde,count,pl"/>
    <form  suffix="es" tag="nounde,count,pl,measure"/>
    <form  suffix="ën" tag="nounde,count,pl"/>
    <form  suffix="ën" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun244" rads=".*(strate|ste|ve)" fast="-">
    <!-- 3 members -->
    <form  suffix="eg" tag="nounde,count,sg"/>
    <form  suffix="gen" tag="nounde,count,pl"/>
  </table>
  <table name="noun245" rads=".*(bestandde|aande|perce)" fast="-">
    <!-- 3 members -->
    <form  suffix="el" tag="nounhet,count,sg"/>
    <form  suffix="el" tag="nounhet,count,sg,measure"/>
    <form  suffix="len" tag="nounhet,count,pl"/>
    <form  suffix="len" tag="nounhet,count,pl,measure"/>
  </table>
  <table name="noun246" rads=".*(oestroge|ve|allerge)" fast="-">
    <!-- 3 members -->
    <form  suffix="en" tag="nounhet,count,sg"/>
    <form  suffix="nen" tag="nounhet,count,pl"/>
  </table>
  <table name="noun247" rads=".*(re|gre|stre)" fast="-">
    <!-- 3 members -->
    <form  suffix="ep" tag="nounde,count,sg"/>
    <form  suffix="ep" tag="nounde,count,sg,measure"/>
    <form  suffix="pen" tag="nounde,count,pl"/>
    <form  suffix="pen" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun248" rads=".*(kor|kol|snui)" fast="-">
    <!-- 3 members -->
    <form  suffix="f" tag="nounde,count,sg"/>
    <form  suffix="f" tag="nounde,count,sg,measure"/>
    <form  suffix="ven" tag="nounde,count,pl"/>
    <form  suffix="ven" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun249" rads=".*(heterose|transse|homose)" fast="-">
    <!-- 3 members -->
    <form  suffix="ksueel" tag="nounde,count,sg"/>
    <form  suffix="ksuelen" tag="nounde,count,pl"/>
    <form  suffix="xueel" tag="nounde,count,sg"/>
    <form  suffix="xuelen" tag="nounde,count,pl"/>
  </table>
  <table name="noun250" rads=".*(slo|klo|sto)" fast="-">
    <!-- 3 members -->
    <form  suffix="of" tag="nounde,count,sg"/>
    <form  suffix="ven" tag="nounde,count,pl"/>
  </table>
  <table name="noun251" rads=".*(pisto|machinepisto|ido)" fast="-">
    <!-- 3 members -->
    <form  suffix="ol" tag="nounhet,count,sg"/>
    <form  suffix="len" tag="nounhet,count,pl"/>
  </table>
  <table name="noun252" rads=".*(bijnierschorshormo|gedragspatro|levenspatro)" fast="-">
    <!-- 3 members -->
    <form  suffix="on" tag="nounhet,count,sg"/>
    <form  suffix="nen" tag="nounhet,count,pl"/>
  </table>
  <table name="noun253" rads=".*(sto|slo|mo)" fast="-">
    <!-- 3 members -->
    <form  suffix="ot" tag="nounde,count,sg"/>
    <form  suffix="ot" tag="nounde,count,sg,measure"/>
    <form  suffix="ten" tag="nounde,count,pl"/>
    <form  suffix="ten" tag="nounde,count,pl,measure"/>
  </table>
  <table name="noun254" rads=".*(glazu|prefectu|figu)" fast="-">
    <!-- 3 members -->
    <form  suffix="ur" tag="nounboth,count,sg"/>
    <form  suffix="ren" tag="nounboth,count,pl"/>
  </table>
  <table name="noun255" rads=".*(doctorand|catalog|cycl)" fast="-">
    <!-- 3 members -->
    <form  suffix="us" tag="nounde,count,sg"/>
    <form  suffix="i" tag="nounde,count,pl"/>
    <form  suffix="ussen" tag="nounde,count,pl"/>
  </table>
  <table name="np_adjective1" rads=".*(eens|oneens|kwijt)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="np_adjectivepp"/>
  </table>
  <table name="np_adjective2" rads=".*(verschuldigd|verplicht|schuldig)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="np_adjectiveso_np"/>
    <form  suffix="" tag="np_adjectiveso_pp"/>
  </table>
  <table name="np_me_adjective1" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="" tag="np_me_adjectiveno_e"/>
  </table>
  <table name="np_me_adjective2" rads=".*[oeai][urne][dkgp]" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="np_me_adjectiveno_e"/>
    <form  suffix="e" tag="np_me_adjectivee"/>
  </table>
  <table name="number1" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="" tag="numberhoofd"/>
  </table>
  <table name="number2" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="numberrang"/>
  </table>
  <table name="p1" rads=".*" fast="-">
    <!-- 21 members -->
    <form  suffix="" tag="ostnp_adverb"/>
  </table>
  <table name="p2" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="" tag="ostn_adverb"/>
  </table>
  <table name="p3" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="" tag="p"/>
  </table>
  <table name="p4" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="" tag="redm_adverb"/>
  </table>
  <table name="p5" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="ostadj_adverb"/>
    <form  suffix="" tag="ostadv_adverb"/>
    <form  suffix="" tag="ostnp_adverb"/>
    <form  suffix="" tag="ostp_adverb"/>
  </table>
  <table name="p6" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="" tag="ost_n_n"/>
  </table>
  <table name="p7" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="" tag="ostadv_adverb"/>
  </table>
  <table name="p8" rads=".*(weer|ongeveer|reeds|maar|al)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="ostadv_adverb"/>
    <form  suffix="" tag="ostnp_adverb"/>
  </table>
  <table name="p9" rads=".*(willekeurig|gelijk|onverschillig|eender|ongeacht)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="re_wh_adverb"/>
  </table>
  <table name="pp1" rads=".*(bovenstrooms|bovendeks|bovengronds)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="ppboven"/>
  </table>
  <table name="pre_det_quant1" rads=".*(allebei|allevijf|allevier|alledrie)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="pre_det_quantallebei"/>
  </table>
  <table name="pred_np_me_adjective1" rads=".*(hogerop|verderop|achterop)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="pred_np_me_adjectivedir_locadv"/>
  </table>
  <table name="preposition1" rads=".*(achterheen|achteruit|achterlangs|achterdoor)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="prepositionachter,extracted_np"/>
  </table>
  <table name="pronoun1" rads=".*(allemaal|allebei|allevijf|allevier|alledrie|alletwee)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="pronounnwh,thi,pl,de,both,indef"/>
  </table>
  <table name="pronoun2" rads=".*(hen|henzelf|hunzelf|elkander)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="pronounnwh,thi,pl,de,dat_acc,def"/>
  </table>
  <table name="pronoun3" rads=".*(men|hijzelf|hij)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="pronounnwh,thi,sg,de,nom,def"/>
  </table>
  <table name="proper_name1" rads=".*">
    <!-- 30 members -->
    <form  suffix="" tag="proper_namesg,'MISC'"/>
  </table>
  <table name="punct1" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="" tag="punctis_gelijk"/>
  </table>
  <table name="punct2" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="punctschuin_streep"/>
  </table>
  <table name="punct3" rads=".*\!" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="punctuitroep"/>
  </table>
  <table name="punct4" rads=".*\?" fast="-">
    <!-- 8 members -->
    <form  suffix="" tag="punctvraag"/>
  </table>
  <table name="punct5" rads=".*(\!\?\!\?|\!\!\?|\?\!|\!\?|\?\!\?\!|\?\!\?)" fast="-">
    <!-- 6 members -->
    <form  suffix="" tag="punctuitroep"/>
    <form  suffix="" tag="punctvraag"/>
  </table>
  <table name="punct6" rads=".*(\,\,|\`\`|«|\&\#8220\;|\`)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="punctaanhaal_links"/>
  </table>
  <table name="punct7" rads=".*(\|\|\|\||\|\|\|\|\||\|\||\||\|\|\|)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="punctstaand_streep"/>
  </table>
  <table name="punct8" rads=".*(\)|\]|\>|\})" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="puncthaak_sluit"/>
  </table>
  <table name="punct9" rads=".*(\{|\[|\<)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="puncthaak_open"/>
  </table>
  <table name="punct10" rads=".*" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="punctligg_streep"/>
  </table>
  <table name="reflexive1" rads=".*(mij|mezelf|me|mijzelf)" fast="-">
    <!-- 4 members -->
    <form  suffix="" tag="reflexivefir,sg"/>
  </table>
  <table name="s1" rads=".*">
    <!-- 83 members -->
    <form  suffix="" tag="entence_adverb"/>
  </table>
  <table name="t1" rads=".*">
    <!-- 224 members -->
    <form  suffix="" tag="ag"/>
  </table>
  <table name="v1" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="" tag="andaar_adverb"/>
  </table>
  <table name="v2" rads=".*(genoeg|voldoende|onvoldoende)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="p_om_adverb"/>
  </table>
  <table name="verb1" rads=".*">
    <!-- 397 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb2" rads=".*[eë]">
    <!-- 285 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="er" tag="verbhebben,sg1"/>
    <form  suffix="erde" tag="verbhebben,past"/>
    <form  suffix="erden" tag="verbhebben,past"/>
    <form  suffix="ert" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="erd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="erd" tag="verbunacc,psp"/>
  </table>
  <table name="verb3" rads=".*">
    <!-- 252 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="er" tag="verbhebben,sg1"/>
    <form  suffix="erde" tag="verbhebben,past"/>
    <form  suffix="erden" tag="verbhebben,past"/>
    <form  suffix="ert" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="erd" tag="verbhebben,psp"/>
  </table>
  <table name="verb4" rads=".*">
    <!-- 241 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="d" tag="verbhebben,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb5" rads=".*">
    <!-- 136 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
  </table>
  <table name="verb6" rads=".*">
    <!-- 106 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
  </table>
  <table name="verb7" rads=".*">
    <!-- 56 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="" tag="verbunacc,psp"/>
  </table>
  <table name="verb8" rads=".*">
    <!-- 55 members -->
    <form  suffix="" tag="verbhebben,inf"/>
  </table>
  <table name="verb9" rads=".*">
    <!-- 55 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="d" tag="verbhebben,psp"/>
    <form  suffix="d" tag="verbunacc,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb10" rads=".*">
    <!-- 52 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
  </table>
  <table name="verb11" rads=".*">
    <!-- 49 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb12" rads=".*">
    <!-- 43 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,psp"/>
    <form  suffix="" tag="verbhebben,sg"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb13" rads=".*">
    <!-- 41 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
  </table>
  <table name="verb14" rads=".*">
    <!-- 31 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="d" tag="verbhebben,psp"/>
    <form  suffix="d" tag="verbunacc,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="de" tag="verbunacc,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbunacc,past"/>
    <form  suffix="en" tag="verbunacc,inf"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
  </table>
  <table name="verb15" rads=".*" fast="-">
    <!-- 29 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,psp"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb16" rads=".*" fast="-">
    <!-- 29 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb17" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="" tag="verbhebben,psp"/>
  </table>
  <table name="verb18" rads=".*" fast="-">
    <!-- 27 members -->
    <form  suffix="pen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
  </table>
  <table name="verb19" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbzijn,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="de" tag="verbzijn,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbzijn,past"/>
    <form  suffix="en" tag="verbzijn,inf"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbzijn,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
    <form prefix="ge" suffix="d" tag="verbzijn,psp"/>
  </table>
  <table name="verb20" rads=".*" fast="-">
    <!-- 25 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb21" rads=".*" fast="-">
    <!-- 20 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="" tag="verbunacc,psp"/>
  </table>
  <table name="verb22" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="en" tag="verbunacc,inf"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="d" tag="verbunacc,psp"/>
    <form  suffix="de" tag="verbunacc,past"/>
    <form  suffix="den" tag="verbunacc,past"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
  </table>
  <table name="verb23" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="sen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
  </table>
  <table name="verb24" rads=".*" fast="-">
    <!-- 19 members -->
    <form  suffix="ten" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="" tag="verbunacc,psp"/>
  </table>
  <table name="verb25" rads=".*" fast="-">
    <!-- 18 members -->
    <form  suffix="ten" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,psp"/>
    <form  suffix="" tag="verbhebben,sg"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb26" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="er" tag="verbhebben,sg1"/>
    <form  suffix="erd" tag="verbhebben,psp"/>
    <form  suffix="erde" tag="verbhebben,past"/>
    <form  suffix="erden" tag="verbhebben,past"/>
    <form  suffix="ert" tag="verbhebben,sg3"/>
  </table>
  <table name="verb27" rads=".*" fast="-">
    <!-- 17 members -->
    <form  suffix="zen" tag="verbhebben,inf"/>
    <form  suffix="s" tag="verbhebben,sg1"/>
    <form  suffix="sde" tag="verbhebben,past"/>
    <form  suffix="sden" tag="verbhebben,past"/>
    <form  suffix="st" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="sd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="sd" tag="verbunacc,psp"/>
  </table>
  <table name="verb28" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="" tag="verbhebben,psp"/>
  </table>
  <table name="verb29" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="ven" tag="verbhebben,inf"/>
    <form  suffix="f" tag="verbhebben,sg1"/>
    <form  suffix="fde" tag="verbhebben,past"/>
    <form  suffix="fden" tag="verbhebben,past"/>
    <form  suffix="ft" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="fd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="fd" tag="verbunacc,psp"/>
  </table>
  <table name="verb30" rads=".*" fast="-">
    <!-- 14 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="d" tag="verbhebben,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb31" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="en" tag="verb'hebben/zijn',inf"/>
    <form  suffix="" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verb'hebben/zijn',past"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verb'hebben/zijn',past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="t" tag="verb'hebben/zijn',sg3"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb32" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="en" tag="verb'hebben/zijn',inf"/>
    <form  suffix="" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="de" tag="verb'hebben/zijn',past"/>
    <form  suffix="den" tag="verb'hebben/zijn',past"/>
    <form  suffix="t" tag="verb'hebben/zijn',sg3"/>
    <form prefix="ge" suffix="d" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb33" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbzijn,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="de" tag="verbzijn,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbzijn,past"/>
    <form  suffix="en" tag="verbzijn,inf"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbzijn,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbzijn,psp"/>
  </table>
  <table name="verb34" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="men" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb35" rads=".*" fast="-">
    <!-- 13 members -->
    <form  suffix="pen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
  </table>
  <table name="verb36" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbzijn,sg1"/>
    <form  suffix="en" tag="verbzijn,inf"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbzijn,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="te" tag="verbzijn,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbzijn,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
    <form prefix="ge" suffix="t" tag="verbzijn,psp"/>
  </table>
  <table name="verb37" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
  </table>
  <table name="verb38" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="sen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
  </table>
  <table name="verb39" rads=".*" fast="-">
    <!-- 12 members -->
    <form  suffix="ten" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="" tag="verbhebben,psp"/>
  </table>
  <table name="verb40" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="" tag="verbzijn,psp"/>
  </table>
  <table name="verb41" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="nen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="d" tag="verbhebben,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb42" rads=".*" fast="-">
    <!-- 11 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="er" tag="verbhebben,sg1"/>
    <form  suffix="er" tag="verbzijn,sg1"/>
    <form  suffix="erde" tag="verbhebben,past"/>
    <form  suffix="erde" tag="verbzijn,past"/>
    <form  suffix="erden" tag="verbhebben,past"/>
    <form  suffix="erden" tag="verbzijn,past"/>
    <form  suffix="ert" tag="verbhebben,sg3"/>
    <form  suffix="ert" tag="verbzijn,sg3"/>
    <form  suffix="ren" tag="verbzijn,inf"/>
    <form prefix="ge" suffix="erd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="erd" tag="verbzijn,psp"/>
  </table>
  <table name="verb43" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="de" tag="verbunacc,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbunacc,past"/>
    <form  suffix="en" tag="verbunacc,inf"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb44" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="en" tag="verbzijn,inf"/>
    <form  suffix="" tag="verbzijn,sg1"/>
    <form  suffix="de" tag="verbzijn,past"/>
    <form  suffix="den" tag="verbzijn,past"/>
    <form  suffix="t" tag="verbzijn,sg3"/>
    <form prefix="ge" suffix="d" tag="verbzijn,psp"/>
  </table>
  <table name="verb45" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="fen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
  </table>
  <table name="verb46" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="ijden" tag="verbhebben,inf"/>
    <form  suffix="eden" tag="verbhebben,past"/>
    <form  suffix="eden" tag="verbhebben,psp"/>
    <form  suffix="eed" tag="verbhebben,past"/>
    <form  suffix="ijd" tag="verbhebben,sg1"/>
    <form  suffix="ijdt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb47" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="pen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb48" rads=".*" fast="-">
    <!-- 10 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="ur" tag="verbhebben,sg1"/>
    <form  suffix="urde" tag="verbhebben,past"/>
    <form  suffix="urden" tag="verbhebben,past"/>
    <form  suffix="urt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="urd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="urd" tag="verbunacc,psp"/>
  </table>
  <table name="verb49" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,psp"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbunacc,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb50" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="en" tag="verbunacc,inf"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="de" tag="verbunacc,past"/>
    <form  suffix="den" tag="verbunacc,past"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb51" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="ak" tag="verbhebben,sg1"/>
    <form  suffix="akt" tag="verbhebben,sg3"/>
    <form  suffix="akte" tag="verbhebben,past"/>
    <form  suffix="akten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="akt" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="akt" tag="verbunacc,psp"/>
  </table>
  <table name="verb52" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="sen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb53" rads=".*" fast="-">
    <!-- 9 members -->
    <form  suffix="ëren" tag="verbhebben,inf"/>
    <form  suffix="eer" tag="verbhebben,sg1"/>
    <form  suffix="eerde" tag="verbhebben,past"/>
    <form  suffix="eerden" tag="verbhebben,past"/>
    <form  suffix="eert" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="eerd" tag="verbhebben,psp"/>
  </table>
  <table name="verb54" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbzijn,sg1"/>
    <form  suffix="d" tag="verbhebben,psp"/>
    <form  suffix="d" tag="verbzijn,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="de" tag="verbzijn,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbzijn,past"/>
    <form  suffix="en" tag="verbzijn,inf"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbzijn,sg3"/>
  </table>
  <table name="verb55" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,psp"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb56" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="el" tag="verbhebben,sg1"/>
    <form  suffix="eld" tag="verbhebben,psp"/>
    <form  suffix="elde" tag="verbhebben,past"/>
    <form  suffix="elden" tag="verbhebben,past"/>
    <form  suffix="elt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb57" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="men" tag="verbhebben,inf"/>
    <form  suffix="am" tag="verbhebben,sg1"/>
    <form  suffix="amd" tag="verbhebben,psp"/>
    <form  suffix="amde" tag="verbhebben,past"/>
    <form  suffix="amden" tag="verbhebben,past"/>
    <form  suffix="amt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb58" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="nen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb59" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb60" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ren" tag="verbzijn,inf"/>
    <form  suffix="er" tag="verbzijn,sg1"/>
    <form  suffix="erde" tag="verbzijn,past"/>
    <form  suffix="erden" tag="verbzijn,past"/>
    <form  suffix="ert" tag="verbzijn,sg3"/>
    <form prefix="ge" suffix="erd" tag="verbzijn,psp"/>
  </table>
  <table name="verb61" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="ven" tag="verbhebben,inf"/>
    <form  suffix="f" tag="verbhebben,sg1"/>
    <form  suffix="fd" tag="verbhebben,psp"/>
    <form  suffix="fde" tag="verbhebben,past"/>
    <form  suffix="fden" tag="verbhebben,past"/>
    <form  suffix="ft" tag="verbhebben,sg3"/>
  </table>
  <table name="verb62" rads=".*" fast="-">
    <!-- 8 members -->
    <form  suffix="zen" tag="verbhebben,inf"/>
    <form  suffix="s" tag="verbhebben,sg1"/>
    <form  suffix="sde" tag="verbhebben,past"/>
    <form  suffix="sden" tag="verbhebben,past"/>
    <form  suffix="st" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="sd" tag="verbhebben,psp"/>
  </table>
  <table name="verb63" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="en" tag="verb'hebben/zijn',inf"/>
    <form  suffix="" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbzijn,sg1"/>
    <form  suffix="de" tag="verb'hebben/zijn',past"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="de" tag="verbzijn,past"/>
    <form  suffix="den" tag="verb'hebben/zijn',past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbzijn,past"/>
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="en" tag="verbzijn,inf"/>
    <form  suffix="t" tag="verb'hebben/zijn',sg3"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbzijn,sg3"/>
    <form prefix="ge" suffix="d" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
    <form prefix="ge" suffix="d" tag="verbzijn,psp"/>
  </table>
  <table name="verb64" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="en" tag="verb'hebben/zijn',inf"/>
    <form  suffix="" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="" tag="verbzijn,sg1"/>
    <form  suffix="de" tag="verb'hebben/zijn',past"/>
    <form  suffix="de" tag="verbzijn,past"/>
    <form  suffix="den" tag="verb'hebben/zijn',past"/>
    <form  suffix="den" tag="verbzijn,past"/>
    <form  suffix="en" tag="verbzijn,inf"/>
    <form  suffix="t" tag="verb'hebben/zijn',sg3"/>
    <form  suffix="t" tag="verbzijn,sg3"/>
    <form prefix="ge" suffix="d" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
    <form prefix="ge" suffix="d" tag="verbzijn,psp"/>
  </table>
  <table name="verb65" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ijven" tag="verbhebben,inf"/>
    <form  suffix="eef" tag="verbhebben,past"/>
    <form  suffix="even" tag="verbhebben,past"/>
    <form  suffix="even" tag="verbhebben,psp"/>
    <form  suffix="ijf" tag="verbhebben,sg1"/>
    <form  suffix="ijft" tag="verbhebben,sg3"/>
  </table>
  <table name="verb66" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ingen" tag="verbhebben,inf"/>
    <form  suffix="ing" tag="verbhebben,sg1"/>
    <form  suffix="ingt" tag="verbhebben,sg3"/>
    <form  suffix="ong" tag="verbhebben,past"/>
    <form  suffix="ongen" tag="verbhebben,past"/>
    <form  suffix="ongen" tag="verbhebben,psp"/>
  </table>
  <table name="verb67" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="ken" tag="verbunacc,inf"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,psp"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="te" tag="verbunacc,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbunacc,past"/>
  </table>
  <table name="verb68" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="d" tag="verbhebben,psp"/>
    <form  suffix="d" tag="verbunacc,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb69" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
  </table>
  <table name="verb70" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="nen" tag="verbhebben,inf"/>
    <form  suffix="on" tag="verbhebben,sg1"/>
    <form  suffix="ond" tag="verbhebben,psp"/>
    <form  suffix="onde" tag="verbhebben,past"/>
    <form  suffix="onden" tag="verbhebben,past"/>
    <form  suffix="ont" tag="verbhebben,sg3"/>
  </table>
  <table name="verb71" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ven" tag="verbhebben,inf"/>
    <form  suffix="f" tag="verbhebben,sg1"/>
    <form  suffix="fde" tag="verbhebben,past"/>
    <form  suffix="fden" tag="verbhebben,past"/>
    <form  suffix="ft" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="fd" tag="verbhebben,psp"/>
  </table>
  <table name="verb72" rads=".*" fast="-">
    <!-- 7 members -->
    <form  suffix="ëren" tag="verbhebben,inf"/>
    <form  suffix="eer" tag="verbhebben,sg1"/>
    <form  suffix="eerde" tag="verbhebben,past"/>
    <form  suffix="eerden" tag="verbhebben,past"/>
    <form  suffix="eert" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="eerd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="eerd" tag="verbunacc,psp"/>
  </table>
  <table name="verb73" rads=".*(verkle|bekle|omkle|ontkle|overre|ontle)" fast="-">
    <!-- 6 members -->
    <form  suffix="den" tag="verbhebben,inf"/>
    <form  suffix="ed" tag="verbhebben,psp"/>
    <form  suffix="ed" tag="verbhebben,sg1"/>
    <form  suffix="edde" tag="verbhebben,past"/>
    <form  suffix="edden" tag="verbhebben,past"/>
    <form  suffix="edt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb74" rads=".*(bespr|onderbr|verbr|doorbr|weerspr|verspr)" fast="-">
    <!-- 6 members -->
    <form  suffix="eken" tag="verbhebben,inf"/>
    <form  suffix="ak" tag="verbhebben,past"/>
    <form  suffix="aken" tag="verbhebben,past"/>
    <form  suffix="eek" tag="verbhebben,sg1"/>
    <form  suffix="eekt" tag="verbhebben,sg3"/>
    <form  suffix="oken" tag="verbhebben,psp"/>
  </table>
  <table name="verb75" rads=".*(ontwricht|verkracht|verzucht|onthecht|overnacht|berust)" fast="-">
    <!-- 6 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,psp"/>
    <form  suffix="" tag="verbhebben,sg"/>
    <form  suffix="" tag="verbunacc,psp"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb76" rads=".*(herbek|gel|bek|bestr|verk|ontw)" fast="-">
    <!-- 6 members -->
    <form  suffix="ijken" tag="verbhebben,inf"/>
    <form  suffix="eek" tag="verbhebben,past"/>
    <form  suffix="eken" tag="verbhebben,past"/>
    <form  suffix="eken" tag="verbhebben,psp"/>
    <form  suffix="ijk" tag="verbhebben,sg1"/>
    <form  suffix="ijkt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb77" rads=".*(misma|bewa|verma|veroorza|vervolma|volma)" fast="-">
    <!-- 6 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="ak" tag="verbhebben,sg1"/>
    <form  suffix="akt" tag="verbhebben,psp"/>
    <form  suffix="akt" tag="verbhebben,sg3"/>
    <form  suffix="akte" tag="verbhebben,past"/>
    <form  suffix="akten" tag="verbhebben,past"/>
  </table>
  <table name="verb78" rads=".*(scan|men|ken|gun|plan|run)" fast="-">
    <!-- 6 members -->
    <form  suffix="nen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
  </table>
  <table name="verb79" rads=".*(klo|wo|ho|clo|tro|lo)" fast="-">
    <!-- 6 members -->
    <form  suffix="nen" tag="verbhebben,inf"/>
    <form  suffix="on" tag="verbhebben,sg1"/>
    <form  suffix="onde" tag="verbhebben,past"/>
    <form  suffix="onden" tag="verbhebben,past"/>
    <form  suffix="ont" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="ond" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="ond" tag="verbunacc,psp"/>
  </table>
  <table name="verb80" rads=".*(overho|doorbo|versto|verho|beho|aanho)" fast="-">
    <!-- 6 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="or" tag="verbhebben,sg1"/>
    <form  suffix="ord" tag="verbhebben,psp"/>
    <form  suffix="orde" tag="verbhebben,past"/>
    <form  suffix="orden" tag="verbhebben,past"/>
    <form  suffix="ort" tag="verbhebben,sg3"/>
  </table>
  <table name="verb81" rads=".*(ommu|verstu|beglu|verdu|bezu|verhu)" fast="-">
    <!-- 6 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="ur" tag="verbhebben,sg1"/>
    <form  suffix="urd" tag="verbhebben,psp"/>
    <form  suffix="urde" tag="verbhebben,past"/>
    <form  suffix="urden" tag="verbhebben,past"/>
    <form  suffix="urt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb82" rads=".*(onttr|overtr|voltr|doortr|omtr)" fast="-">
    <!-- 5 members -->
    <form  suffix="ekken" tag="verbhebben,inf"/>
    <form  suffix="ek" tag="verbhebben,sg1"/>
    <form  suffix="ekt" tag="verbhebben,sg3"/>
    <form  suffix="ok" tag="verbhebben,past"/>
    <form  suffix="okken" tag="verbhebben,past"/>
    <form  suffix="okken" tag="verbhebben,psp"/>
  </table>
  <table name="verb83" rads=".*(verscherp|verwerkelijk|ontmenselijk|verdamp|verziek)" fast="-">
    <!-- 5 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="en" tag="verbunacc,inf"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,psp"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="te" tag="verbunacc,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbunacc,past"/>
  </table>
  <table name="verb84" rads=".*(geschied|verzand|beland|ontaard|ontbrand)" fast="-">
    <!-- 5 members -->
    <form  suffix="en" tag="verbunacc,inf"/>
    <form  suffix="" tag="verbunacc,psp"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="de" tag="verbunacc,past"/>
    <form  suffix="den" tag="verbunacc,past"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
  </table>
  <table name="verb85" rads=".*(ged|bed|doord|verd|overd)" fast="-">
    <!-- 5 members -->
    <form  suffix="enken" tag="verbhebben,inf"/>
    <form  suffix="acht" tag="verbhebben,past"/>
    <form  suffix="acht" tag="verbhebben,psp"/>
    <form  suffix="achten" tag="verbhebben,past"/>
    <form  suffix="enk" tag="verbhebben,sg1"/>
    <form  suffix="enkt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb86" rads=".*(schaf|grif|pof|stof|logenstraf)" fast="-">
    <!-- 5 members -->
    <form  suffix="fen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
  </table>
  <table name="verb87" rads=".*(bela|gewa|verda|bekla|verla)" fast="-">
    <!-- 5 members -->
    <form  suffix="gen" tag="verbhebben,inf"/>
    <form  suffix="ag" tag="verbhebben,sg1"/>
    <form  suffix="agd" tag="verbhebben,psp"/>
    <form  suffix="agde" tag="verbhebben,past"/>
    <form  suffix="agden" tag="verbhebben,past"/>
    <form  suffix="agt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb88" rads=".*(doorz|bez|overz|herz|ontz)" fast="-">
    <!-- 5 members -->
    <form  suffix="ien" tag="verbhebben,inf"/>
    <form  suffix="ag" tag="verbhebben,past"/>
    <form  suffix="agen" tag="verbhebben,past"/>
    <form  suffix="ie" tag="verbhebben,sg1"/>
    <form  suffix="ien" tag="verbhebben,psp"/>
    <form  suffix="iene" tag="verbhebben,inf"/>
    <form  suffix="iet" tag="verbhebben,sg3"/>
  </table>
  <table name="verb89" rads=".*(verb|herv|onderv|ontb|versl)" fast="-">
    <!-- 5 members -->
    <form  suffix="inden" tag="verbhebben,inf"/>
    <form  suffix="ind" tag="verbhebben,sg1"/>
    <form  suffix="indt" tag="verbhebben,sg3"/>
    <form  suffix="ond" tag="verbhebben,past"/>
    <form  suffix="onden" tag="verbhebben,past"/>
    <form  suffix="onden" tag="verbhebben,psp"/>
  </table>
  <table name="verb90" rads=".*(overha|achterha|ontha|verha|bestra)" fast="-">
    <!-- 5 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="al" tag="verbhebben,sg1"/>
    <form  suffix="ald" tag="verbhebben,psp"/>
    <form  suffix="alde" tag="verbhebben,past"/>
    <form  suffix="alden" tag="verbhebben,past"/>
    <form  suffix="alt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb91" rads=".*(sma|ka|fa|ba|dra)" fast="-">
    <!-- 5 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="al" tag="verbhebben,sg1"/>
    <form  suffix="alde" tag="verbhebben,past"/>
    <form  suffix="alden" tag="verbhebben,past"/>
    <form  suffix="alt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="ald" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="ald" tag="verbunacc,psp"/>
  </table>
  <table name="verb92" rads=".*(sche|kwe|zinspe|de|oorde)" fast="-">
    <!-- 5 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="el" tag="verbhebben,sg1"/>
    <form  suffix="elde" tag="verbhebben,past"/>
    <form  suffix="elden" tag="verbhebben,past"/>
    <form  suffix="elt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="eld" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="eld" tag="verbunacc,psp"/>
  </table>
  <table name="verb93" rads=".*(vermom|verdom|overstem|omklem|bestem)" fast="-">
    <!-- 5 members -->
    <form  suffix="men" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="d" tag="verbhebben,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb94" rads=".*(sche|ze|stre|zwe|dwe)" fast="-">
    <!-- 5 members -->
    <form  suffix="pen" tag="verbhebben,inf"/>
    <form  suffix="ep" tag="verbhebben,sg1"/>
    <form  suffix="ept" tag="verbhebben,sg3"/>
    <form  suffix="epte" tag="verbhebben,past"/>
    <form  suffix="epten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="ept" tag="verbhebben,psp"/>
  </table>
  <table name="verb95" rads=".*(revalide|galoppe|ve|demarre|marche)" fast="-">
    <!-- 5 members -->
    <form  suffix="ren" tag="verb'hebben/zijn',inf"/>
    <form  suffix="er" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="erde" tag="verb'hebben/zijn',past"/>
    <form  suffix="erden" tag="verb'hebben/zijn',past"/>
    <form  suffix="ert" tag="verb'hebben/zijn',sg3"/>
    <form prefix="ge" suffix="erd" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="erd" tag="verbunacc,psp"/>
  </table>
  <table name="verb96" rads=".*(sco|glo|bo|sto|ho)" fast="-">
    <!-- 5 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="or" tag="verbhebben,sg1"/>
    <form  suffix="orde" tag="verbhebben,past"/>
    <form  suffix="orden" tag="verbhebben,past"/>
    <form  suffix="ort" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="ord" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="ord" tag="verbunacc,psp"/>
  </table>
  <table name="verb97" rads=".*(arrive|reste|acclimatise|stagne|krepe)" fast="-">
    <!-- 5 members -->
    <form  suffix="ren" tag="verbunacc,inf"/>
    <form  suffix="er" tag="verbunacc,sg1"/>
    <form  suffix="erde" tag="verbunacc,past"/>
    <form  suffix="erden" tag="verbunacc,past"/>
    <form  suffix="ert" tag="verbunacc,sg3"/>
    <form prefix="ge" suffix="erd" tag="verbunacc,psp"/>
  </table>
  <table name="verb98" rads=".*(verdr|gedr|misdr|bedr)" fast="-">
    <!-- 4 members -->
    <form  suffix="agen" tag="verbhebben,inf"/>
    <form  suffix="aag" tag="verbhebben,sg1"/>
    <form  suffix="aagt" tag="verbhebben,sg3"/>
    <form  suffix="agen" tag="verbhebben,psp"/>
    <form  suffix="oeg" tag="verbhebben,past"/>
    <form  suffix="oegen" tag="verbhebben,past"/>
  </table>
  <table name="verb99" rads=".*(verv|onderv|verh|bev)" fast="-">
    <!-- 4 members -->
    <form  suffix="angen" tag="verbhebben,inf"/>
    <form  suffix="ang" tag="verbhebben,sg1"/>
    <form  suffix="angen" tag="verbhebben,psp"/>
    <form  suffix="angt" tag="verbhebben,sg3"/>
    <form  suffix="ing" tag="verbhebben,past"/>
    <form  suffix="ingen" tag="verbhebben,past"/>
  </table>
  <table name="verb100" rads=".*(krab|dub|schrob|tob)" fast="-">
    <!-- 4 members -->
    <form  suffix="ben" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb101" rads=".*(klad|red|wed|schud)" fast="-">
    <!-- 4 members -->
    <form  suffix="den" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="" tag="verbunacc,psp"/>
  </table>
  <table name="verb102" rads=".*(hink|zwalk|schaats|zwalp)" fast="-">
    <!-- 4 members -->
    <form  suffix="en" tag="verb'hebben/zijn',inf"/>
    <form  suffix="" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="t" tag="verb'hebben/zijn',sg3"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verb'hebben/zijn',past"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verb'hebben/zijn',past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
  </table>
  <table name="verb103" rads=".*(verbluf|overbluf|besef|bestraf)" fast="-">
    <!-- 4 members -->
    <form  suffix="fen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb104" rads=".*(kna|weekla|pla|za)" fast="-">
    <!-- 4 members -->
    <form  suffix="gen" tag="verbhebben,inf"/>
    <form  suffix="ag" tag="verbhebben,sg1"/>
    <form  suffix="agde" tag="verbhebben,past"/>
    <form  suffix="agden" tag="verbhebben,past"/>
    <form  suffix="agt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="agd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="agd" tag="verbunacc,psp"/>
  </table>
  <table name="verb105" rads=".*(bo|lo|ho|po)" fast="-">
    <!-- 4 members -->
    <form  suffix="gen" tag="verbhebben,inf"/>
    <form  suffix="og" tag="verbhebben,sg1"/>
    <form  suffix="ogde" tag="verbhebben,past"/>
    <form  suffix="ogden" tag="verbhebben,past"/>
    <form  suffix="ogt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="ogd" tag="verbhebben,psp"/>
  </table>
  <table name="verb106" rads=".*(besch|verg|overg|beg)" fast="-">
    <!-- 4 members -->
    <form  suffix="ieten" tag="verbhebben,inf"/>
    <form  suffix="iet" tag="verbhebben,sg"/>
    <form  suffix="oot" tag="verbhebben,past"/>
    <form  suffix="oten" tag="verbhebben,past"/>
    <form  suffix="oten" tag="verbhebben,psp"/>
  </table>
  <table name="verb107" rads=".*(best|overst|verzw|herkr)" fast="-">
    <!-- 4 members -->
    <form  suffix="ijgen" tag="verbhebben,inf"/>
    <form  suffix="eeg" tag="verbhebben,past"/>
    <form  suffix="egen" tag="verbhebben,past"/>
    <form  suffix="egen" tag="verbhebben,psp"/>
    <form  suffix="ijg" tag="verbhebben,sg1"/>
    <form  suffix="ijgt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb108" rads=".*(p|n|sl|gr)" fast="-">
    <!-- 4 members -->
    <form  suffix="ijpen" tag="verbhebben,inf"/>
    <form  suffix="eep" tag="verbhebben,past"/>
    <form  suffix="epen" tag="verbhebben,past"/>
    <form  suffix="ijp" tag="verbhebben,sg1"/>
    <form  suffix="ijpt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="epen" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="epen" tag="verbunacc,psp"/>
  </table>
  <table name="verb109" rads=".*(b|sm|sch|kr)" fast="-">
    <!-- 4 members -->
    <form  suffix="ijten" tag="verbhebben,inf"/>
    <form  suffix="eet" tag="verbhebben,past"/>
    <form  suffix="eten" tag="verbhebben,past"/>
    <form  suffix="ijt" tag="verbhebben,sg"/>
    <form prefix="ge" suffix="eten" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="eten" tag="verbunacc,psp"/>
  </table>
  <table name="verb110" rads=".*(wr|d|dw|z)" fast="-">
    <!-- 4 members -->
    <form  suffix="ingen" tag="verbhebben,inf"/>
    <form  suffix="ing" tag="verbhebben,sg1"/>
    <form  suffix="ingt" tag="verbhebben,sg3"/>
    <form  suffix="ong" tag="verbhebben,past"/>
    <form  suffix="ongen" tag="verbhebben,past"/>
    <form prefix="ge" suffix="ongen" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="ongen" tag="verbunacc,psp"/>
  </table>
  <table name="verb111" rads=".*(knak|zwak|plak|stok)" fast="-">
    <!-- 4 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="ken" tag="verbunacc,inf"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="te" tag="verbunacc,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbunacc,past"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
  </table>
  <table name="verb112" rads=".*(beschik|verhapstuk|verpak|verblik)" fast="-">
    <!-- 4 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,psp"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb113" rads=".*(wra|noodza|la|sla)" fast="-">
    <!-- 4 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="ak" tag="verbhebben,sg1"/>
    <form  suffix="akt" tag="verbhebben,sg3"/>
    <form  suffix="akte" tag="verbhebben,past"/>
    <form  suffix="akten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="akt" tag="verbhebben,psp"/>
  </table>
  <table name="verb114" rads=".*(sto|ro|stro|spo)" fast="-">
    <!-- 4 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="ok" tag="verbhebben,sg1"/>
    <form  suffix="okt" tag="verbhebben,sg3"/>
    <form  suffix="okte" tag="verbhebben,past"/>
    <form  suffix="okten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="okt" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="okt" tag="verbunacc,psp"/>
  </table>
  <table name="verb115" rads=".*(verzak|geluk|verongeluk|misluk)" fast="-">
    <!-- 4 members -->
    <form  suffix="ken" tag="verbunacc,inf"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="t" tag="verbunacc,psp"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
    <form  suffix="te" tag="verbunacc,past"/>
    <form  suffix="ten" tag="verbunacc,past"/>
  </table>
  <table name="verb116" rads=".*(tol|hol|bol|bal)" fast="-">
    <!-- 4 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbzijn,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="de" tag="verbzijn,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbzijn,past"/>
    <form  suffix="len" tag="verbzijn,inf"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbzijn,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
    <form prefix="ge" suffix="d" tag="verbzijn,psp"/>
  </table>
  <table name="verb117" rads=".*(ke|stre|ve|te)" fast="-">
    <!-- 4 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="el" tag="verbhebben,sg1"/>
    <form  suffix="elde" tag="verbhebben,past"/>
    <form  suffix="elden" tag="verbhebben,past"/>
    <form  suffix="elt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="eld" tag="verbhebben,psp"/>
  </table>
  <table name="verb118" rads=".*(overstro|omzo|verchro|doorstro)" fast="-">
    <!-- 4 members -->
    <form  suffix="men" tag="verbhebben,inf"/>
    <form  suffix="om" tag="verbhebben,sg1"/>
    <form  suffix="omd" tag="verbhebben,psp"/>
    <form  suffix="omde" tag="verbhebben,past"/>
    <form  suffix="omden" tag="verbhebben,past"/>
    <form  suffix="omt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb119" rads=".*(to|ro|dwarsbo|schro)" fast="-">
    <!-- 4 members -->
    <form  suffix="men" tag="verbhebben,inf"/>
    <form  suffix="om" tag="verbhebben,sg1"/>
    <form  suffix="omde" tag="verbhebben,past"/>
    <form  suffix="omden" tag="verbhebben,past"/>
    <form  suffix="omt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="omd" tag="verbhebben,psp"/>
  </table>
  <table name="verb120" rads=".*(verban|bespan|omspan|overspan)" fast="-">
    <!-- 4 members -->
    <form  suffix="nen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="nen" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb121" rads=".*(verle|bewe|ontle|vere)" fast="-">
    <!-- 4 members -->
    <form  suffix="nen" tag="verbhebben,inf"/>
    <form  suffix="en" tag="verbhebben,sg1"/>
    <form  suffix="end" tag="verbhebben,psp"/>
    <form  suffix="ende" tag="verbhebben,past"/>
    <form  suffix="enden" tag="verbhebben,past"/>
    <form  suffix="ent" tag="verbhebben,sg3"/>
  </table>
  <table name="verb122" rads=".*(scha|spa|ba|pa)" fast="-">
    <!-- 4 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="ar" tag="verbhebben,sg1"/>
    <form  suffix="arde" tag="verbhebben,past"/>
    <form  suffix="arden" tag="verbhebben,past"/>
    <form  suffix="art" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="ard" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="ard" tag="verbunacc,psp"/>
  </table>
  <table name="verb123" rads=".*(verklo|verlo|begro|ontblo)" fast="-">
    <!-- 4 members -->
    <form  suffix="ten" tag="verbhebben,inf"/>
    <form  suffix="ot" tag="verbhebben,psp"/>
    <form  suffix="ot" tag="verbhebben,sg"/>
    <form  suffix="otte" tag="verbhebben,past"/>
    <form  suffix="otten" tag="verbhebben,past"/>
  </table>
  <table name="verb124" rads=".*(bedr|besl|bekr|bez)" fast="-">
    <!-- 4 members -->
    <form  suffix="uipen" tag="verbhebben,inf"/>
    <form  suffix="oop" tag="verbhebben,past"/>
    <form  suffix="open" tag="verbhebben,past"/>
    <form  suffix="open" tag="verbhebben,psp"/>
    <form  suffix="uip" tag="verbhebben,sg1"/>
    <form  suffix="uipt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb125" rads=".*(handha|boeksta|la|sta)" fast="-">
    <!-- 4 members -->
    <form  suffix="ven" tag="verbhebben,inf"/>
    <form  suffix="af" tag="verbhebben,sg1"/>
    <form  suffix="afde" tag="verbhebben,past"/>
    <form  suffix="afden" tag="verbhebben,past"/>
    <form  suffix="aft" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="afd" tag="verbhebben,psp"/>
  </table>
  <table name="verb126" rads=".*(doorle|doorze|bele|herbele)" fast="-">
    <!-- 4 members -->
    <form  suffix="ven" tag="verbhebben,inf"/>
    <form  suffix="ef" tag="verbhebben,sg1"/>
    <form  suffix="efd" tag="verbhebben,psp"/>
    <form  suffix="efde" tag="verbhebben,past"/>
    <form  suffix="efden" tag="verbhebben,past"/>
    <form  suffix="eft" tag="verbhebben,sg3"/>
  </table>
  <table name="verb127" rads=".*(slo|lo|ro|klo)" fast="-">
    <!-- 4 members -->
    <form  suffix="ven" tag="verbhebben,inf"/>
    <form  suffix="of" tag="verbhebben,sg1"/>
    <form  suffix="ofde" tag="verbhebben,past"/>
    <form  suffix="ofden" tag="verbhebben,past"/>
    <form  suffix="oft" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="ofd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="ofd" tag="verbunacc,psp"/>
  </table>
  <table name="verb128" rads=".*(verst|doorst|weerst)" fast="-">
    <!-- 3 members -->
    <form  suffix="aan" tag="verbhebben,inf"/>
    <form  suffix="a" tag="verbhebben,sg1"/>
    <form  suffix="aan" tag="verbhebben,psp"/>
    <form  suffix="aat" tag="verbhebben,sg3"/>
    <form  suffix="ane" tag="verbhebben,inf"/>
    <form  suffix="ond" tag="verbhebben,past"/>
    <form  suffix="onden" tag="verbhebben,past"/>
  </table>
  <table name="verb129" rads=".*(verr|ontr|ber)" fast="-">
    <!-- 3 members -->
    <form  suffix="aden" tag="verbhebben,inf"/>
    <form  suffix="aad" tag="verbhebben,sg1"/>
    <form  suffix="aadde" tag="verbhebben,past"/>
    <form  suffix="aadden" tag="verbhebben,past"/>
    <form  suffix="aadt" tag="verbhebben,sg3"/>
    <form  suffix="aden" tag="verbhebben,psp"/>
    <form  suffix="ied" tag="verbhebben,past"/>
    <form  suffix="ieden" tag="verbhebben,past"/>
  </table>
  <table name="verb130" rads=".*(verbrod|beklad|verwed)" fast="-">
    <!-- 3 members -->
    <form  suffix="den" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,psp"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb131" rads=".*(ba|scha|zonneba)" fast="-">
    <!-- 3 members -->
    <form  suffix="den" tag="verbhebben,inf"/>
    <form  suffix="ad" tag="verbhebben,sg1"/>
    <form  suffix="adde" tag="verbhebben,past"/>
    <form  suffix="adden" tag="verbhebben,past"/>
    <form  suffix="adt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="ad" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="ad" tag="verbunacc,psp"/>
  </table>
  <table name="verb132" rads=".*(verzo|doorzo|bezo)" fast="-">
    <!-- 3 members -->
    <form  suffix="eken" tag="verbhebben,inf"/>
    <form  suffix="cht" tag="verbhebben,past"/>
    <form  suffix="cht" tag="verbhebben,psp"/>
    <form  suffix="chten" tag="verbhebben,past"/>
    <form  suffix="ek" tag="verbhebben,sg1"/>
    <form  suffix="ekt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb133" rads=".*(ben|ontn|vern)" fast="-">
    <!-- 3 members -->
    <form  suffix="emen" tag="verbhebben,inf"/>
    <form  suffix="am" tag="verbhebben,past"/>
    <form  suffix="amen" tag="verbhebben,past"/>
    <form  suffix="eem" tag="verbhebben,sg1"/>
    <form  suffix="eemt" tag="verbhebben,sg3"/>
    <form  suffix="omen" tag="verbhebben,psp"/>
  </table>
  <table name="verb134" rads=".*(switch|gulp|koers)" fast="-">
    <!-- 3 members -->
    <form  suffix="en" tag="verb'hebben/zijn',inf"/>
    <form  suffix="" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="t" tag="verb'hebben/zijn',sg3"/>
    <form  suffix="te" tag="verb'hebben/zijn',past"/>
    <form  suffix="ten" tag="verb'hebben/zijn',past"/>
    <form prefix="ge" suffix="t" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
  </table>
  <table name="verb135" rads=".*(verhard|verbrand|vervreemd)" fast="-">
    <!-- 3 members -->
    <form  suffix="en" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,psp"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbunacc,psp"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="de" tag="verbunacc,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbunacc,past"/>
    <form  suffix="en" tag="verbunacc,inf"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
  </table>
  <table name="verb136" rads=".*(verstedelijk|verzakelijk|verwelk)" fast="-">
    <!-- 3 members -->
    <form  suffix="en" tag="verbunacc,inf"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="t" tag="verbunacc,psp"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
    <form  suffix="te" tag="verbunacc,past"/>
    <form  suffix="ten" tag="verbunacc,past"/>
  </table>
  <table name="verb137" rads=".*(crash|finish|floep)" fast="-">
    <!-- 3 members -->
    <form  suffix="en" tag="verbzijn,inf"/>
    <form  suffix="" tag="verbzijn,sg1"/>
    <form  suffix="t" tag="verbzijn,sg3"/>
    <form  suffix="te" tag="verbzijn,past"/>
    <form  suffix="ten" tag="verbzijn,past"/>
    <form prefix="ge" suffix="t" tag="verbzijn,psp"/>
  </table>
  <table name="verb138" rads=".*(verw|onderw|ontw)" fast="-">
    <!-- 3 members -->
    <form  suffix="erpen" tag="verbhebben,inf"/>
    <form  suffix="erp" tag="verbhebben,sg1"/>
    <form  suffix="erpt" tag="verbhebben,sg3"/>
    <form  suffix="ierp" tag="verbhebben,past"/>
    <form  suffix="ierpen" tag="verbhebben,past"/>
    <form  suffix="orpen" tag="verbhebben,psp"/>
  </table>
  <table name="verb139" rads=".*(verg|herg|beg)" fast="-">
    <!-- 3 members -->
    <form  suffix="even" tag="verbhebben,inf"/>
    <form  suffix="af" tag="verbhebben,past"/>
    <form  suffix="aven" tag="verbhebben,past"/>
    <form  suffix="eef" tag="verbhebben,sg1"/>
    <form  suffix="eeft" tag="verbhebben,sg3"/>
    <form  suffix="even" tag="verbhebben,psp"/>
    <form  suffix="even" tag="verbunacc,psp"/>
  </table>
  <table name="verb140" rads=".*(verleg|overbrug|weerleg)" fast="-">
    <!-- 3 members -->
    <form  suffix="gen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="d" tag="verbhebben,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
  </table>
  <table name="verb141" rads=".*(zigzag|plag|vlag)" fast="-">
    <!-- 3 members -->
    <form  suffix="gen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb142" rads=".*(beha|versa|beraadsla)" fast="-">
    <!-- 3 members -->
    <form  suffix="gen" tag="verbhebben,inf"/>
    <form  suffix="ag" tag="verbhebben,sg1"/>
    <form  suffix="agd" tag="verbhebben,psp"/>
    <form  suffix="agd" tag="verbunacc,psp"/>
    <form  suffix="agde" tag="verbhebben,past"/>
    <form  suffix="agden" tag="verbhebben,past"/>
    <form  suffix="agt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb143" rads=".*(beo|verho|beto)" fast="-">
    <!-- 3 members -->
    <form  suffix="gen" tag="verbhebben,inf"/>
    <form  suffix="og" tag="verbhebben,sg1"/>
    <form  suffix="ogd" tag="verbhebben,psp"/>
    <form  suffix="ogde" tag="verbhebben,past"/>
    <form  suffix="ogden" tag="verbhebben,past"/>
    <form  suffix="ogt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb144" rads=".*(zo|knipo|zielto)" fast="-">
    <!-- 3 members -->
    <form  suffix="gen" tag="verbhebben,inf"/>
    <form  suffix="og" tag="verbhebben,sg1"/>
    <form  suffix="ogde" tag="verbhebben,past"/>
    <form  suffix="ogden" tag="verbhebben,past"/>
    <form  suffix="ogt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="ogd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="ogd" tag="verbunacc,psp"/>
  </table>
  <table name="verb145" rads=".*(n|r|zw)" fast="-">
    <!-- 3 members -->
    <form  suffix="ijgen" tag="verbhebben,inf"/>
    <form  suffix="eeg" tag="verbhebben,past"/>
    <form  suffix="egen" tag="verbhebben,past"/>
    <form  suffix="ijg" tag="verbhebben,sg1"/>
    <form  suffix="ijgt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="egen" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="egen" tag="verbunacc,psp"/>
  </table>
  <table name="verb146" rads=".*(r|sp|kw)" fast="-">
    <!-- 3 members -->
    <form  suffix="ijten" tag="verbhebben,inf"/>
    <form  suffix="eet" tag="verbhebben,past"/>
    <form  suffix="eten" tag="verbhebben,past"/>
    <form  suffix="ijt" tag="verbhebben,sg"/>
    <form prefix="ge" suffix="eten" tag="verbhebben,psp"/>
  </table>
  <table name="verb147" rads=".*(kl|dr|bl)" fast="-">
    <!-- 3 members -->
    <form  suffix="inken" tag="verbhebben,inf"/>
    <form  suffix="ink" tag="verbhebben,sg1"/>
    <form  suffix="inkt" tag="verbhebben,sg3"/>
    <form  suffix="onk" tag="verbhebben,past"/>
    <form  suffix="onken" tag="verbhebben,past"/>
    <form prefix="ge" suffix="onken" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="onken" tag="verbunacc,psp"/>
  </table>
  <table name="verb148" rads=".*(ontg|herw|ontsp)" fast="-">
    <!-- 3 members -->
    <form  suffix="innen" tag="verbhebben,inf"/>
    <form  suffix="in" tag="verbhebben,sg1"/>
    <form  suffix="int" tag="verbhebben,sg3"/>
    <form  suffix="on" tag="verbhebben,past"/>
    <form  suffix="onnen" tag="verbhebben,past"/>
    <form  suffix="onnen" tag="verbhebben,psp"/>
  </table>
  <table name="verb149" rads=".*(stik|lek|rek)" fast="-">
    <!-- 3 members -->
    <form  suffix="ken" tag="verb'hebben/zijn',inf"/>
    <form  suffix="" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="ken" tag="verbunacc,inf"/>
    <form  suffix="t" tag="verb'hebben/zijn',sg3"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
    <form  suffix="te" tag="verb'hebben/zijn',past"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="te" tag="verbunacc,past"/>
    <form  suffix="ten" tag="verb'hebben/zijn',past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbunacc,past"/>
    <form prefix="ge" suffix="t" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
  </table>
  <table name="verb150" rads=".*(schik|knik|pak)" fast="-">
    <!-- 3 members -->
    <form  suffix="ken" tag="verb'hebben/zijn',inf"/>
    <form  suffix="" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="t" tag="verb'hebben/zijn',sg3"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="te" tag="verb'hebben/zijn',past"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verb'hebben/zijn',past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="t" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="t" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="t" tag="verbunacc,psp"/>
  </table>
  <table name="verb151" rads=".*(sme|kwe|pre)" fast="-">
    <!-- 3 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="ek" tag="verbhebben,sg1"/>
    <form  suffix="ekt" tag="verbhebben,sg3"/>
    <form  suffix="ekte" tag="verbhebben,past"/>
    <form  suffix="ekten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="ekt" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="ekt" tag="verbunacc,psp"/>
  </table>
  <table name="verb152" rads=".*(bewiero|besto|versto)" fast="-">
    <!-- 3 members -->
    <form  suffix="ken" tag="verbhebben,inf"/>
    <form  suffix="ok" tag="verbhebben,sg1"/>
    <form  suffix="okt" tag="verbhebben,psp"/>
    <form  suffix="okt" tag="verbhebben,sg3"/>
    <form  suffix="okte" tag="verbhebben,past"/>
    <form  suffix="okten" tag="verbhebben,past"/>
  </table>
  <table name="verb153" rads=".*(versmal|versnel|verkil)" fast="-">
    <!-- 3 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="" tag="verbunacc,sg1"/>
    <form  suffix="d" tag="verbhebben,psp"/>
    <form  suffix="d" tag="verbunacc,psp"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="de" tag="verbunacc,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbunacc,past"/>
    <form  suffix="len" tag="verbunacc,inf"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,sg3"/>
  </table>
  <table name="verb154" rads=".*(bepa|herha|verta)" fast="-">
    <!-- 3 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="al" tag="verbhebben,sg1"/>
    <form  suffix="ald" tag="verbhebben,psp"/>
    <form  suffix="ald" tag="verbunacc,psp"/>
    <form  suffix="alde" tag="verbhebben,past"/>
    <form  suffix="alden" tag="verbhebben,past"/>
    <form  suffix="alt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb155" rads=".*(sta|ta|scha)" fast="-">
    <!-- 3 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="al" tag="verbhebben,sg1"/>
    <form  suffix="alde" tag="verbhebben,past"/>
    <form  suffix="alden" tag="verbhebben,past"/>
    <form  suffix="alt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="ald" tag="verbhebben,psp"/>
  </table>
  <table name="verb156" rads=".*(verve|verde|beoorde)" fast="-">
    <!-- 3 members -->
    <form  suffix="len" tag="verbhebben,inf"/>
    <form  suffix="el" tag="verbhebben,sg1"/>
    <form  suffix="eld" tag="verbhebben,psp"/>
    <form  suffix="eld" tag="verbunacc,psp"/>
    <form  suffix="elde" tag="verbhebben,past"/>
    <form  suffix="elden" tag="verbhebben,past"/>
    <form  suffix="elt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb157" rads=".*(vlam|slalom|ram)" fast="-">
    <!-- 3 members -->
    <form  suffix="men" tag="verb'hebben/zijn',inf"/>
    <form  suffix="" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verb'hebben/zijn',past"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verb'hebben/zijn',past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="men" tag="verbhebben,inf"/>
    <form  suffix="t" tag="verb'hebben/zijn',sg3"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb158" rads=".*(som|kam|tem)" fast="-">
    <!-- 3 members -->
    <form  suffix="men" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
  </table>
  <table name="verb159" rads=".*(kra|scha|ra)" fast="-">
    <!-- 3 members -->
    <form  suffix="men" tag="verbhebben,inf"/>
    <form  suffix="am" tag="verbhebben,sg1"/>
    <form  suffix="amde" tag="verbhebben,past"/>
    <form  suffix="amden" tag="verbhebben,past"/>
    <form  suffix="amt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="amd" tag="verbhebben,psp"/>
  </table>
  <table name="verb160" rads=".*(dro|bo|dagdro)" fast="-">
    <!-- 3 members -->
    <form  suffix="men" tag="verbhebben,inf"/>
    <form  suffix="om" tag="verbhebben,sg1"/>
    <form  suffix="omde" tag="verbhebben,past"/>
    <form  suffix="omden" tag="verbhebben,past"/>
    <form  suffix="omt" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="omd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="omd" tag="verbunacc,psp"/>
  </table>
  <table name="verb161" rads=".*(barbecue|recycle|manage)" fast="-">
    <!-- 3 members -->
    <form  suffix="n" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="de" tag="verbhebben,past"/>
    <form  suffix="den" tag="verbhebben,past"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="d" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="d" tag="verbunacc,psp"/>
  </table>
  <table name="verb162" rads=".*(ba|wa|ma)" fast="-">
    <!-- 3 members -->
    <form  suffix="nen" tag="verbhebben,inf"/>
    <form  suffix="an" tag="verbhebben,sg1"/>
    <form  suffix="ande" tag="verbhebben,past"/>
    <form  suffix="anden" tag="verbhebben,past"/>
    <form  suffix="ant" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="and" tag="verbhebben,psp"/>
  </table>
  <table name="verb163" rads=".*(overl|doorl|bel)" fast="-">
    <!-- 3 members -->
    <form  suffix="open" tag="verbhebben,inf"/>
    <form  suffix="iep" tag="verbhebben,past"/>
    <form  suffix="iepen" tag="verbhebben,past"/>
    <form  suffix="oop" tag="verbhebben,sg1"/>
    <form  suffix="oopt" tag="verbhebben,sg3"/>
    <form  suffix="open" tag="verbhebben,psp"/>
  </table>
  <table name="verb164" rads=".*(weerh|beh|verh)" fast="-">
    <!-- 3 members -->
    <form  suffix="ouden" tag="verbhebben,inf"/>
    <form  suffix="ield" tag="verbhebben,past"/>
    <form  suffix="ielden" tag="verbhebben,past"/>
    <form  suffix="oud" tag="verbhebben,sg1"/>
    <form  suffix="ouden" tag="verbhebben,psp"/>
    <form  suffix="oudt" tag="verbhebben,sg3"/>
  </table>
  <table name="verb165" rads=".*(verkrap|verpop|overlap)" fast="-">
    <!-- 3 members -->
    <form  suffix="pen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,psp"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb166" rads=".*(ka|schra|ga)" fast="-">
    <!-- 3 members -->
    <form  suffix="pen" tag="verbhebben,inf"/>
    <form  suffix="ap" tag="verbhebben,sg1"/>
    <form  suffix="apt" tag="verbhebben,sg3"/>
    <form  suffix="apte" tag="verbhebben,past"/>
    <form  suffix="apten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="apt" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="apt" tag="verbunacc,psp"/>
  </table>
  <table name="verb167" rads=".*(versle|onderstre|versche)" fast="-">
    <!-- 3 members -->
    <form  suffix="pen" tag="verbhebben,inf"/>
    <form  suffix="ep" tag="verbhebben,sg1"/>
    <form  suffix="ept" tag="verbhebben,psp"/>
    <form  suffix="ept" tag="verbhebben,sg3"/>
    <form  suffix="epte" tag="verbhebben,past"/>
    <form  suffix="epten" tag="verbhebben,past"/>
  </table>
  <table name="verb168" rads=".*(ho|do|no)" fast="-">
    <!-- 3 members -->
    <form  suffix="pen" tag="verbhebben,inf"/>
    <form  suffix="op" tag="verbhebben,sg1"/>
    <form  suffix="opt" tag="verbhebben,sg3"/>
    <form  suffix="opte" tag="verbhebben,past"/>
    <form  suffix="opten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="opt" tag="verbhebben,psp"/>
  </table>
  <table name="verb169" rads=".*(wanho|slo|stro)" fast="-">
    <!-- 3 members -->
    <form  suffix="pen" tag="verbhebben,inf"/>
    <form  suffix="op" tag="verbhebben,sg1"/>
    <form  suffix="opt" tag="verbhebben,sg3"/>
    <form  suffix="opte" tag="verbhebben,past"/>
    <form  suffix="opten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="opt" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="opt" tag="verbunacc,psp"/>
  </table>
  <table name="verb170" rads=".*(passe|date|promove)" fast="-">
    <!-- 3 members -->
    <form  suffix="ren" tag="verb'hebben/zijn',inf"/>
    <form  suffix="er" tag="verb'hebben/zijn',sg1"/>
    <form  suffix="er" tag="verbhebben,sg1"/>
    <form  suffix="er" tag="verbzijn,sg1"/>
    <form  suffix="erde" tag="verb'hebben/zijn',past"/>
    <form  suffix="erde" tag="verbhebben,past"/>
    <form  suffix="erde" tag="verbzijn,past"/>
    <form  suffix="erden" tag="verb'hebben/zijn',past"/>
    <form  suffix="erden" tag="verbhebben,past"/>
    <form  suffix="erden" tag="verbzijn,past"/>
    <form  suffix="ert" tag="verb'hebben/zijn',sg3"/>
    <form  suffix="ert" tag="verbhebben,sg3"/>
    <form  suffix="ert" tag="verbzijn,sg3"/>
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="ren" tag="verbzijn,inf"/>
    <form prefix="ge" suffix="erd" tag="verb'hebben/zijn',psp"/>
    <form prefix="ge" suffix="erd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="erd" tag="verbzijn,psp"/>
  </table>
  <table name="verb171" rads=".*(bespa|geba|bewa)" fast="-">
    <!-- 3 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="ar" tag="verbhebben,sg1"/>
    <form  suffix="ard" tag="verbhebben,psp"/>
    <form  suffix="ard" tag="verbunacc,psp"/>
    <form  suffix="arde" tag="verbhebben,past"/>
    <form  suffix="arden" tag="verbhebben,past"/>
    <form  suffix="art" tag="verbhebben,sg3"/>
  </table>
  <table name="verb172" rads=".*(verga|bezwa|ontwa)" fast="-">
    <!-- 3 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="ar" tag="verbhebben,sg1"/>
    <form  suffix="ard" tag="verbhebben,psp"/>
    <form  suffix="arde" tag="verbhebben,past"/>
    <form  suffix="arden" tag="verbhebben,past"/>
    <form  suffix="art" tag="verbhebben,sg3"/>
  </table>
  <table name="verb173" rads=".*(kalme|stabilise|kristallise)" fast="-">
    <!-- 3 members -->
    <form  suffix="ren" tag="verbhebben,inf"/>
    <form  suffix="er" tag="verbhebben,sg1"/>
    <form  suffix="er" tag="verbunacc,sg1"/>
    <form  suffix="erde" tag="verbhebben,past"/>
    <form  suffix="erde" tag="verbunacc,past"/>
    <form  suffix="erden" tag="verbhebben,past"/>
    <form  suffix="erden" tag="verbunacc,past"/>
    <form  suffix="ert" tag="verbhebben,sg3"/>
    <form  suffix="ert" tag="verbunacc,sg3"/>
    <form  suffix="ren" tag="verbunacc,inf"/>
    <form prefix="ge" suffix="erd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="erd" tag="verbunacc,psp"/>
  </table>
  <table name="verb174" rads=".*(verkas|verras|beslis)" fast="-">
    <!-- 3 members -->
    <form  suffix="sen" tag="verbhebben,inf"/>
    <form  suffix="" tag="verbhebben,sg1"/>
    <form  suffix="t" tag="verbhebben,psp"/>
    <form  suffix="t" tag="verbhebben,sg3"/>
    <form  suffix="t" tag="verbunacc,psp"/>
    <form  suffix="te" tag="verbhebben,past"/>
    <form  suffix="ten" tag="verbhebben,past"/>
  </table>
  <table name="verb175" rads=".*(pra|bla|ba)" fast="-">
    <!-- 3 members -->
    <form  suffix="ten" tag="verbhebben,inf"/>
    <form  suffix="at" tag="verbhebben,sg"/>
    <form  suffix="atte" tag="verbhebben,past"/>
    <form  suffix="atten" tag="verbhebben,past"/>
    <form prefix="ge" suffix="at" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="at" tag="verbunacc,psp"/>
  </table>
  <table name="verb176" rads=".*(ontsl|besp|omsl)" fast="-">
    <!-- 3 members -->
    <form  suffix="uiten" tag="verbhebben,inf"/>
    <form  suffix="oot" tag="verbhebben,past"/>
    <form  suffix="oten" tag="verbhebben,past"/>
    <form  suffix="oten" tag="verbhebben,psp"/>
    <form  suffix="uit" tag="verbhebben,sg"/>
  </table>
  <table name="verb177" rads=".*(sne|kle|be)" fast="-">
    <!-- 3 members -->
    <form  suffix="ven" tag="verbhebben,inf"/>
    <form  suffix="ef" tag="verbhebben,sg1"/>
    <form  suffix="efde" tag="verbhebben,past"/>
    <form  suffix="efden" tag="verbhebben,past"/>
    <form  suffix="eft" tag="verbhebben,sg3"/>
    <form prefix="ge" suffix="efd" tag="verbhebben,psp"/>
    <form prefix="ge" suffix="efd" tag="verbunacc,psp"/>
  </table>
  <table name="verb178" rads=".*(bedroe|vertoe|behoe)" fast="-">
    <!-- 3 members -->
    <form  suffix="ven" tag="verbhebben,inf"/>
    <form  suffix="f" tag="verbhebben,sg1"/>
    <form  suffix="fd" tag="verbhebben,psp"/>
    <form  suffix="fd" tag="verbunacc,psp"/>
    <form  suffix="fde" tag="verbhebben,past"/>
    <form  suffix="fden" tag="verbhebben,past"/>
    <form  suffix="ft" tag="verbhebben,sg3"/>
  </table>
  <table name="verb179" rads=".*(gelo|belo|verlo)" fast="-">
    <!-- 3 members -->
    <form  suffix="ven" tag="verbhebben,inf"/>
    <form  suffix="of" tag="verbhebben,sg1"/>
    <form  suffix="ofd" tag="verbhebben,psp"/>
    <form  suffix="ofd" tag="verbunacc,psp"/>
    <form  suffix="ofde" tag="verbhebben,past"/>
    <form  suffix="ofden" tag="verbhebben,past"/>
    <form  suffix="oft" tag="verbhebben,sg3"/>
  </table>
  <table name="verb180" rads=".*(begren|berei|vergui)" fast="-">
    <!-- 3 members -->
    <form  suffix="zen" tag="verbhebben,inf"/>
    <form  suffix="s" tag="verbhebben,sg1"/>
    <form  suffix="sd" tag="verbhebben,psp"/>
    <form  suffix="sde" tag="verbhebben,past"/>
    <form  suffix="sden" tag="verbhebben,past"/>
    <form  suffix="st" tag="verbhebben,sg3"/>
  </table>
  <table name="w1" rads=".*(hoever|hoezeer|hoezo|hoeverre|wanneer)" fast="-">
    <!-- 5 members -->
    <form  suffix="" tag="h_adverb"/>
  </table>
  <table name="waar_adverb1" rads=".*(waarnaartoe|waarnaar|waarheen)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="waar_adverbnaar"/>
  </table>
  <table name="waar_adverb2" rads=".*(waartegen|waartegenaan|waartegenop)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="waar_adverbtegen"/>
  </table>
  <table name="waar_adverb3" rads=".*(vanwaar|waarvandaan|waarvan)" fast="-">
    <!-- 3 members -->
    <form  suffix="" tag="waar_adverbvan"/>
  </table>
  <table name="with_dt1" rads=".*" fast="-">
    <!-- 15 members -->
    <form  suffix="" tag="with_dtetc,dt,vg,0,1)])"/>
  </table>
</description>
